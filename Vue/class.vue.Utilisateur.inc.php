<?php
/*=============================================================================================================
	Fichier				: class.vue.Home.inc.php
	Auteur				: Christophe Goidin (christophe.goidin@ac-grenoble.fr)
	Date de création	: Aout 2013
	Date de modification: 
	Rôle				: Affichage de la page d'accueil du site et de la page d'erreur
===============================================================================================================*/

/**
 * Classe de la vue Home
 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 * @version 1.0
 * @copyright Christophe Goidin - Août 2013
 */
class VueUtilisateur {
	// =====================================================================================================================================================
	// Les attributs
	// =====================================================================================================================================================
 		/* aucun attribut */
	    
	// =====================================================================================================================================================
	// Page Accueil - Les méthodes destinées à la génération du contenu XHTML
	// =====================================================================================================================================================
	/**
     * Renvoie le contenu du bloc XHTML relatif à la présentation de l'association CinePassion38
     * @static
     * @param string $pTitre : le titre du bloc
     * @param string $pContenu : le contenu du bloc	
     * @return string : le contenu XHTML relatif à la présentation de l'association
     * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
     * @version 1.0
     * @copyright Christophe Goidin - Août 2013
     */
	
	public static function GetXhtmlAuthentification($pInfos){
		if($pInfos['ResultAuthentification']){
			$Result = "<img src='./Image/Divers/Ok.png'><span style='color: #00CC00'>Authentification réussie</span>";
			$Result .= "<ul><li>";
			if($pInfos['Sexe'] == 'H'){
				$Result .= "Monsieur ";
			}
			else{
				$Result .= "Madame ";
			}
			$Result .= $pInfos['NomPrenom'].", Dernière connexion : ".$pInfos['DateDerniereConnexion'];
			$Result .= "</li>";
			$Result .= "<li>Vous avez un profil de type ".$pInfos['Type']."</li>";
			$Result .= "<li>Tentatives : ".$pInfos['NbEchecConnexion']."</li>";
			$Result .= "</ul>";
		}
		else{
			$Result = "<img src='./Image/Divers/Ko.png'><span style='color: #CC0000'>Echec de la tentative d'authentification</span>";
			$Result .= "<ul><li>Le login ou le mot de passe saisi est incorrect. Veuillez essayer à nouveau en saisissant votre login et votre mot de passe à partir du formulaire d'authentification</li>";
			$Result .= "<li>Si vous avez oublié vos identifiants...</li>";
			$Result .= "<li>Si vous n'êtes pas encore inscrit, ...</li></ul>";
		}
		return $Result;
	}
	
	public static function GetXhtmlAccueil($pInfos){
		return $pInfos['TexteAccueil'];
	}

	public static function GetXhtmlFormMotDePasse($pVue){
		return $pVue['Formulaire'];
	}
	
	// =====================================================================================================================================================
	// Page d'erreur - Les méthodes destinées à la génération du contenu XHTML
	// =====================================================================================================================================================
	/**
     * Renvoie le contenu du bloc XHTML relatif à la page d'erreur
     * @static
     * @param string $pContenu : le contenu de la page d'erreur
     * @return string : le contenu XHTML relatif à la page d'erreur
     * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
     * @version 1.0
     * @copyright Christophe Goidin - Août 2013
     */
	
	public static function GetXhtmlErreur($pContenu) {
    	return "<span class='Centrer'><img alt='En construction' src='./Image/Divers/PageEnConstruction.png' /></span>".$pContenu;
    }   
	
} // fin class

?>