<?php
/*=============================================================================================================
	Fichier				: class.vue.Film.inc.php
	Auteur				:
	Date de création	:
	Date de modification: 
	Rôle				: Affichage du tableau des films de la page Liste des films
===============================================================================================================*/

/**
 * Classe de la vue Film
 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 * @version 1.0
 * @copyright Christophe Goidin - Août 2013
 */
class VueFilm {
	// =====================================================================================================================================================
	// Les attributs
	// =====================================================================================================================================================
 		/* aucun attribut */
	    
	// =====================================================================================================================================================
	// Page Accueil - Les méthodes destinées à la génération du contenu XHTML
	// =====================================================================================================================================================
    /**
     * Renvoie le contenu du bloc XHTML relatif à la présentation de l'association CinePassion38
     * @static
     * @param string $pContenu : le contenu du bloc
     * @return string : le contenu XHTML relatif à la présentation de l'association
     * @internal param string $pTitre : le titre du bloc
     * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
     * @version 1.0
     * @copyright Christophe Goidin - Août 2013
     */
	public static function GetXhtmlCinePassion38($pContenu) {
    	return $pContenu['TexteAccueil'];
	}
	
	// =====================================================================================================================================================
	// Page Accueil - Les méthodes destinées à la génération du contenu XHTML
	// =====================================================================================================================================================
	/**
	 * Renvoie le contenu du bloc XHTML relatif à la présentation des films
	 * @static
	 * @param string $pGalerie : la galerie des affiches
	 * @return string : le contenu XHTML relatif à la présentation des films
	 * @author Clément Vitti
	 * @version 1.0
	 * @copyright Clément Vitti
	 */
	
	public static function GetXhtmlSlideShow($pGalerie){
		$Galerie = "<div id='slideshow'>";
		foreach($pGalerie as $Num => $unFilm){
			if($Num == 0){
				$Galerie .= '<div class="active"><img alt="'.$unFilm.'" src="'.DIR_FILM_AFFICHE."/".$unFilm.'"></div>';
			}
			else{
				$Galerie .= '<div><img alt="'.$unFilm.'" src="'.DIR_FILM_AFFICHE."/".$unFilm.'"></div>';
			}
		}
		return $Galerie."</div>" ;
	}
	
	// =====================================================================================================================================================
	// Page Liste des films - Les méthodes destinées à la génération du contenu XHTML
	// =====================================================================================================================================================
    /**
     * Renvoie le tableau qui liste tous les films disponibles
     * @static
     * @param $pContenu
     * @return string
     * @internal param string $ptabTexte : Le tableau contenant les textes
     * @author Etienne Blanc-Coquand
     * @version 1.0
     * @copyright Etienne Blanc-Coquand - Septembre 2016
     */
	public static function GetXhtmlTableauListeFilms($pContenu) {
		 $Chaine = "<table id='ListeFilm'>
						<tr>
							<th>Les films disponibles (".$pContenu['NbFilms']." films)</th>
							<th>Page n° ".$pContenu['NumPage']."/".$pContenu['NbPages']."</th>
							<th colspan='2'>Films n°".$pContenu['NumPremierFilm']." à ".$pContenu['NumDernierFilm']."</th>
						</tr>
						<tr>
							<th>Titre</th>
							<th>Genre</th>
							<th>Année</th>
							<th>Durée</th>
						</tr>";
		foreach ($pContenu['ListeFilms'] as $unFilm) {
			$Chaine .= "<tr>";
			$Chaine .= "<td title='Un film de ".utf8_encode($unFilm['PrenomPersonne'])." ".utf8_encode($unFilm['NomPersonne'])."'><a href='./Index.php?Page=Film&amp;Action=AfficherInformations&amp;NumPage=".$pContenu['NumPage']."&amp;NumFilm=".$unFilm['NumFilm']."'>".utf8_encode($unFilm['TitreFilm'])."</a></td>";
			$Chaine .= "<td>".utf8_encode($unFilm['LibelleGenre'])."</td>";
			$Chaine .= "<td>".utf8_encode($unFilm['Date'])."</td>";
			$Chaine .= "<td>".utf8_encode($unFilm['DureeFilm'])."</td>";
			$Chaine .= "</tr>";
		}
		For($i=count($pContenu['ListeFilms']); $i<NAV_NB_LIGNESPARPAGE;$i++){
			$Chaine .= "<tr> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td></tr>";
		}
		$Chaine .="</table>";
		return $Chaine;
	}
	
	// =====================================================================================================================================================
	// Génération du code de la barre de navigation - Les méthodes destinées à la génération du contenu XHTML
	// =====================================================================================================================================================
    /**
     * Renvoie le code XHTML des boutons et numéros de navigation
     * @static
     * @param string $pBoutons : Le code des boutons
     * @param string $pNumeros : Le code des numéros
     * @author Etienne Blanc-Coquand
     * @version 1.0
     * @copyright Etienne Blanc-Coquand - Septembre 2016
     * @return string
     */
	
	public static function GetXhtmlNavigationListeFilms($pBoutons, $pNumeros){
		$Result = "<div id='Navigation'>
					<div id='Boutons'>".$pBoutons."</div>
					<div id='Numeros'>".$pNumeros."</div>
				   </div>";
		return $Result;
	}

	// =====================================================================================================================================================
	// Génération du code de l'affiche de l'image - Les méthodes destinées à la génération du contenu XHTML
	// =====================================================================================================================================================
    /**
     * Renvoie le code XHTML l'affiche du film
     * @static
     * @param string $pTitreFilm : Le nom du film
     * @author Etienne Blanc-Coquand
     * @version 1.0
     * @copyright Etienne Blanc-Coquand - Septembre 2016
     * @return string
     */
	
	public static function GetImageFilm($pTitreFilm){
		$Resultat = "<img src='".DIR_FILM_AFFICHE."/".$pTitreFilm.".jpg' />";
		return $Resultat;
	}
	
	// =====================================================================================================================================================
	// Génération du code de l'affiche de l'image - Les méthodes destinées à la génération du contenu XHTML
	// =====================================================================================================================================================
    /**
     * Renvoie le code XHTML l'affiche du film
     * @static
     * @param $pContenu
     * @return string
     * @internal param string $pTitreFilm : Le nom du film
     * @author Etienne Blanc-Coquand
     * @version 1.0
     * @copyright Etienne Blanc-Coquand - Septembre 2016
     */
	
	public static function GetTexteOnglet($pContenu){
		return $pContenu['Informations']."<br/><br/>".utf8_encode($pContenu['Histoire']);
	}
	
	// =====================================================================================================================================================
	// Génération du code de l'affiche de l'image - Les méthodes destinées à la génération du contenu XHTML
	// =====================================================================================================================================================
	/**
	 * Renvoie le code XHTML l'affiche du film
	 * @static
	 * @param string $pTitreFilm: Le nom du film
	 * @author Etienne Blanc-Coquand
	 * @version 1.0
	 * @copyright Etienne Blanc-Coquand - Septembre 2016
	 */
	
	/*public static function GetListeActeurs($pContenu){
		$Chaine = "Liste des acteurs : <br/>";
		foreach ($pContenu['Acteurs'] as $unActeur) {
			$Chaine .= $unActeur['NomPersonne']." ".$unActeur['PrenomPersonne']." est né le ".$unActeur['DateNaissancePersonne']." (".$unActeur['Age']." ans)"."<br/>";
		}
		return $Chaine;
	}*/

    // =====================================================================================================================================================
    // Génération de la liste déroulante - Les méthodes destinées à la génération du contenu XHTML
    // =====================================================================================================================================================
    /**
     * Renvoie le code XHTML de la liste déroulante
     * @static
     * @param string $pTitreFilm : Le nom du film
     * @author Etienne Blanc-Coquand
     * @version 1.0
     * @copyright Etienne Blanc-Coquand - Avril 2017
     * @return string
     */

    public static function GetXhtmlListeFilmsCommuns($pTitreFilm){
        $Resultat = "<div id='listeChoixFilmsCommuns'>
                    <form method='get' action='./Index.php?Page=Film&amp;Action=AfficherResultatsFilmsCommuns'>
                        <select name='choixFilmCommun'>
                        
                        </select>
                        <input type='submit' value='Voir les films en communs' id='btnSendFilmsCommuns'/>
                    </form>
                </div>";
        return $Resultat;
    }

	// =====================================================================================================================================================
	// Page d'erreur - Les méthodes destinées à la génération du contenu XHTML
	// =====================================================================================================================================================
	/**
     * Renvoie le contenu du bloc XHTML relatif à la page d'erreur
     * @static
     * @param string $pContenu : le contenu de la page d'erreur
     * @return string : le contenu XHTML relatif à la page d'erreur
     * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
     * @version 1.0
     * @copyright Christophe Goidin - Août 2013
     */
	
	public static function GetXhtmlErreur($pContenu) {
    	return "<span class='Centrer'><img alt='En construction' src='".DIR_IMAGE_DIVERS."PageEnConstruction.png' /></span>".$pContenu;
    }
	
} // fin class