<?php
/*=============================================================================================================
	Fichier				: class.vue.Pages.inc.php
	Auteur				: Christophe Goidin (christophe.goidin@ac-grenoble.fr)
	Date de création	: Mai 2013
	Date de modification:  
	Rôle				: Décrit l'ensemble des classes permettant la génération des pages Web (les vues)
===============================================================================================================*/

/**
 * La classe BandeauHaut permet de gérer le bandeau haut du site (header)
 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 * @version 1.0
 * @copyright Christophe Goidin - Mai 2013
 */
class BandeauHaut {
	
	// =====================================================================================================================================================
	// Les attributs
	// =====================================================================================================================================================
	private $Authentification;		// L'authentification des visiteurs du site
	private $CheminDeFer;			// Le chemin de fer (navigation entre les pages)
	private $Titre;					// Le titre de la page
	private $Version;				// La version
	private $DateDuJour;			// La date du jour
	private $TexteDefilant;			// Le texte défilant
	private $TabAction;				// Le tableau à 2 dimensions permettant de gérer l'affichage du chemin de fer en fonction des paramètres Page (première dimension) et Action (seconde dimension) récupérés dans l'URL 
	
	
	// =====================================================================================================================================================
	// Le constructeur
	// =====================================================================================================================================================
	/**
	 * Le constructeur permet d'hydrater tous les attributs de la classe BandeauHaut
	 * @param array $pData : le tableau comportant toutes les informations
	 * @return null
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	public function __construct($pData) {
		$this->SetListeDesActions();
		// Hydratation de tous les attributs de la classe en appelant les mutateurs appropriés
		foreach ($pData as $Cle => $Valeur) {
			$Methode = 'Set'.$Cle;
            if (is_callable(array($this, $Methode))) { // teste si la méthode $Methode est "appelable" dans la classe courante ($this) 
            	$this->$Methode($Valeur);
            }
		}
	}
 	
 	
 	// =====================================================================================================================================================
	// Les accesseurs (ou getter)
	// =====================================================================================================================================================
	/**
	 * Renvoie le contenu de l'authentification
	 * @param null 
	 * @return string : le contenu relatif à l'authentification 
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	private function GetAuthentification() {
			if (isset($_SESSION['Login'])){
			return "<img src='".DIR_IMAGE_DIVERS."Personne.png' alt='img' /> ".$_SESSION['Prenom']." ".$_SESSION['Nom']." (".$_SESSION['Type'].") <br/> <a href='./Index.php?Page=Utilisateur&Action=Deconnexion'>Se déconnecter</a>";
		}
		else{
			return $this->Authentification;
		}
	}
	
		
    /**
	 * Renvoie le chemin de fer
	 * @param null 
	 * @return string : le chemin de fer (navigation entre les pages) 
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	private function GetCheminDeFer() {
    	return $this->CheminDeFer;
   	}
   	
	/**
	 * Renvoie le titre de la page
	 * @param null 
	 * @return string : le titre de la page 
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
   	private function GetTitre() {
    	return $this->Titre;
   	}
   	
	/**
	 * Renvoie la version du site
	 * @param null 
	 * @return string : la version du site 
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
   	private function GetVersion() {
    	return $this->Version;
   	}
   	
	/**
	 * Renvoie la date du jour
	 * @param null 
	 * @return string : la date du jour sous forme d'une chaîne de caractères
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Août 2013
	 */
   	private function GetDateduJour() {
    	return $this->DateDuJour;
   	}
   	
	/**
	 * Renvoie le texte défilant
	 * @param null 
	 * @return string : le texte défilant 
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Juillet 2013
	 */
   	private function GetTexteDefilant() {
    	return $this->TexteDefilant;
   	}
   	
	/**
	 * Renvoie le libellé de l'action à afficher dans le chemin de fer
	 * @param string $pPage : le libellé de la page récupérée dans l'URL
	 * @param string $pAction : le libellé de l'action récupéré dans l'URL 
	 * @return string : le texte à afficher dans le chemin de fer correspondant au libellé de l'action récupéré dans l'URL
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Septembre 2013
	 */
   	private function GetLibelleAction($pPage, $pAction) {
    	if (!isset($this->TabAction[$pPage][$pAction])) {
    		return $pAction;
    	}else {
    		return $this->TabAction[$pPage][$pAction];
    	}
   		
   	}

   	
   	// =====================================================================================================================================================
	// Les mutateurs (ou setter)
	// =====================================================================================================================================================
	/**
	 * Positionne le contenu de l'authentification
	 * @param string $pValue : le contenu relatif à l'authentification
	 * @return null
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
   	private function SetAuthentification($pValue) {
   		$this->Authentification = $pValue;
   	}
    
    /**
	 * Positionne le chemin de fer
	 * @param string $pValue : le chemin de fer (navigation entre les pages)
	 * @return null
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Juillet 2013
	 */
 	private function SetCheminDeFer($pPage) {
 		// =====================================================================================================================================================
		// Premier chargement en mode "normal"
		// =====================================================================================================================================================
 		if (strlen($_SERVER['QUERY_STRING']) == 0) {
 			$this->CheminDeFer = "Accueil";
 		
 		// =====================================================================================================================================================
		// Premier chargement en mode "débogage"
		// =====================================================================================================================================================
 		}elseif (substr($_SERVER['QUERY_STRING'], 0, 20) == "XDEBUG_SESSION_START") {
 			$this->CheminDeFer = "Accueil";
 		
 		// =====================================================================================================================================================
		// Cas général
		// =====================================================================================================================================================
 		}else {
 			foreach (explode("&", $_SERVER['QUERY_STRING']) as $Valeur) {
 				$UnParam = explode("=", $Valeur);
 				$TabParam[$UnParam[0]] = $UnParam[1];
 			}
 			if (!isset($TabParam['Page'])) {
 				$this->CheminDeFer = "Accueil - Cas impossible normalement";
 			}else {
 				if ($TabParam['Page'] == "Home") {
 					$this->CheminDeFer = "Accueil";
 				}elseif ($TabParam['Page'] == "CinePassion38") {
 					$this->CheminDeFer = "<a href='./Index.php?Page=Home&amp;Action=AfficherAccueil'>Accueil</a> > ".$TabParam['Page']." > ".$this->GetLibelleAction($TabParam['Page'], $TabParam['Action']);
 				}else {
 					$this->CheminDeFer = "<a href='./Index.php?Page=Home&amp;Action=AfficherAccueil'>Accueil</a> > <a href='./Index.php?Page=".$TabParam['Page']."&amp;Action=Accueil'>".$TabParam['Page']."</a> > ".$this->GetLibelleAction($TabParam['Page'], $TabParam['Action']);
 				} 
 			}
 		}
 	}
 	
	/**
	 * Positionne le titre de la page
	 * @param string $pValue : le titre de la page
	 * @return null
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
 	private function SetTitre($pValue) {
 		$this->Titre = $pValue;
 	}
 	
	/**
	 * Positionne la version du site
	 * @param string $pValue : la version du site
	 * @return null
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
 	private function SetVersion($pValue) {
 		$this->Version = $pValue;
 	}
 	
	/**
	 * Positionne la date du jour
	 * @param string $pValue : la date du jour sous forme d'une chaîne de caractères
	 * @return null
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Août 2013
	 */
 	private function SetDateDuJour($pValue) {
 		$this->DateDuJour = $pValue;
 	}

    /**
     * Positionne le texte défilant
     * @param string $pNomFichier : le fichier qui contient le texte défilant
     * @return null
     * @throws Exception
     * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
     * @version 1.0
     * @copyright Christophe Goidin - Juillet 2013
     */
 	private function SetTexteDefilant($pNomFichier) {
 		if (($RefFichier = fopen($pNomFichier, "r")) !== False) {
 			$TexteDefilant = "<ul id='TexteDefilant'>";
 			// Lecture du titre
 			fgets($RefFichier);
 			for ($i=0; $i<11; $i++) {
 				fgetc($RefFichier);
 			}
 			if (($Titre = fgets($RefFichier)) !== false) {
 				$TexteDefilant .= "<li>$Titre</li>";
 			}
 			fgets($RefFichier);
 			
 			// Lecture du texte qui doit d�filer
			while (!feof($RefFichier)) {
          		$Ligne = fgets($RefFichier);
          		$Tab = explode("#", $Ligne);
          		$TexteDefilant .= "<li><span class='Titre'>".$Tab[0]."</span>".$Tab[1]."</li>";
        	}
        	$TexteDefilant .= "</ul>";
			fclose($RefFichier);
			$this->TexteDefilant = $TexteDefilant;
		}else {
			throw new Exception("Impossible de lire le fichier texte. Le fichier ".$pNomFichier." est introuvable.", 0);
		}
 	}
 	
 	/**
	 * Initialise le tableau faisant la correspondance entre le nom des actions et ce qui s'affiche dans le chemin de fer
	 * @param null
	 * @return null
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Août 2013
	 */
 	private function SetListeDesActions() {
 		$this->TabAction = Array("CinePassion38"	=> Array("Partenaires"			=> "Nos partenaires",
 															 "Plan"					=> "Plan",
 															 "Presentation"			=> "Présentation de l'association")
 								);
 		$this->TabAction = Array("Film"		=>Array("Accueil"				=> "Présentation du module",
 													"AfficherInformations"	=> "Fiche descriptive",
 													"AfficherLaListe"		=> "Liste des films")
 								);
 		$this->TabAction = Array("Utilisateur"		=>Array("Accueil"				=> "Accueil",
 															"Authentification"		=> "Authentification d'un utilisateur",
 															"Inscription"			=> "Inscription d'un utilisateur",
 															"Deconnexion"			=> "Déconnexion d'un utilisateur",
 															"SuppressionCompte"		=> "Suppression d'un compte utilisateur",
 															"ModifierInfos"			=> "Modification des informations personnelles",
 															"ModifierMotDePasse"	=> "Modification du mot de passe")
 								
 								);
 	}
 	
 	
 	// =====================================================================================================================================================
	// Les méthodes relatives à la génération du contenu XHTML
	// =====================================================================================================================================================
	/**
	 * Renvoie le bloc XHTML relatif au bandeau haut de la page comprenant le bandeau lui-même procédé des liens
	 * @param null
	 * @return string : le bloc XHTML relatif au bandeau haut de la page 
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
 	public function GetXhtml() {
		return $this->GetXhtmlLiens().$this->GetXhtmlBandeau();
	}
	
	/**
	 * Renvoie le bloc XHTML relatif aux liens
	 * @param null
	 * @return string : le bloc XHTML relatif aux liens 
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	private function GetXhtmlLiens() {
		$Chaine = "<div id='Liens'>";
		$Chaine .= "<img alt='France' src='".DIR_IMAGE_DIVERS."DrapeauFrance.png' />&nbsp;&nbsp;";
		if (defined("CONF_PARTENAIRE_1_NOM") AND defined("CONF_PARTENAIRE_1_SITE")) {
			$Chaine .= " <a onclick='window.open(this.href); return false;' href='".CONF_PARTENAIRE_1_SITE."'>".CONF_PARTENAIRE_1_NOM."</a>";
		}
		if (defined("CONF_PARTENAIRE_2_NOM") AND defined("CONF_PARTENAIRE_2_SITE")) {
			$Chaine .= " | <a onclick='window.open(this.href); return false;' href='".CONF_PARTENAIRE_2_SITE."'>".CONF_PARTENAIRE_2_NOM."</a>";
		}
		if (defined("CONF_PARTENAIRE_3_NOM") AND defined("CONF_PARTENAIRE_3_SITE")) {
			$Chaine .= " | <a onclick='window.open(this.href); return false;' href='".CONF_PARTENAIRE_3_SITE."'>".CONF_PARTENAIRE_3_NOM."</a>";
		}
		if (defined("CONF_PARTENAIRE_4_NOM") AND defined("CONF_PARTENAIRE_4_SITE")) {
			$Chaine .= " | <a onclick='window.open(this.href); return false;' href='".CONF_PARTENAIRE_4_SITE."'>".CONF_PARTENAIRE_4_NOM."</a>";
		}
		$Chaine .= "</div>\n";
		return $Chaine;
	}
	
	/**
	 * Renvoie le bloc XHTML relatif au bandeau haut
	 * @param null
	 * @return string : le bloc XHTML relatif au bandeau haut
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	private function GetXhtmlBandeau() {
		$Result = "<div id='BandeauHaut'>
						<img alt='' src='".DIR_IMAGE_DIVERS."Fond.jpg'/>
						<img alt='' src='".DIR_IMAGE_DIVERS."CinePassion38Logo.png' id='CinePassion38' />";
		if(isset($_SESSION['Infos']['Login'])){						
			$Result .= '<div id="MessageVerrou"></div>
						<div id="Verrou"><a href="./Index.php?Page=Utilisateur&Action=Deconnexion">
						<img alt="" id="ImgVerrou" src="'.DIR_IMAGE_DIVERS.'VerrouFermer.png"
						onmouseover="window.document.getElementById(\'ImgVerrou\').src=\''.DIR_IMAGE_DIVERS.'VerrouOuvert.png\'; 
					    			window.document.getElementById(\'MessageVerrou\').innerHTML=\'se déconnecter\'"
						onmouseout="window.document.getElementById(\'ImgVerrou\').src=\''.DIR_IMAGE_DIVERS.'VerrouFermer.png\'; 
								    window.document.getElementById(\'MessageVerrou\').innerHTML=\'\' "/>
						</a></div>';
			if($_SESSION['Infos']['Type'] == 'Administrateur'){
				$Result .= '<div id="CleAdmin"><img alt="" id="ImgCle" src="'.DIR_IMAGE_DIVERS.'Administrateur.png"></div>';
			}
		}
		
		$Result .= "<div id='Authentification'>".$this->GetAuthentification()."</div>
						<div id='Titre'>".$this->GetTitre()."</div>
						<div id='CheminDeFer'>Localisation : ".$this->GetCheminDeFer()."</div>
						<div id='Version'>".$this->GetVersion()."</div>
						<div id='DateDuJour'>".$this->GetDateduJour()."</div>";
		if (!is_null($this->GetTexteDefilant())) {
			$Result .= $this->GetTexteDefilant().
					   "<script type=\"text/javascript\">$(function(){  $(\"ul#TexteDefilant\").liScroll({travelocity: 0.02}); });</script>";
		}
		$Result .= "</div>\n";
		return $Result;
	}
	   	 
 	/**
 	 * Méthode MAGIQUE appelée automatiquement lorsque l'utilisateur essaie d'afficher un objet de la classe. La méthode GetXhtml() est appelée.
 	 * @param null
 	 * @return string : le bloc XHTML relatif au bandeau haut de la page 
 	 * @author : Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 	 * @version : 1.0
 	 * @copyright Christophe Goidin - Mai 2013
 	 */
 	public function __toString() {
        return $this->GetXhtml();
    }   

} // fin class


/**
 * La classe BandeauBas permet de gérer le bandeau bas du site (footer)
 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 * @version 1.1
 * @copyright Christophe Goidin - Mai 2013
 */
class BandeauBas {
	
	// =====================================================================================================================================================
	// Les attributs
	// =====================================================================================================================================================
	private $Copyright;		// Le libellé du copyright
		
	// =====================================================================================================================================================
	// Le constructeur
	// =====================================================================================================================================================
    /**
     * Le constructeur permet d'hydrater tous les attributs de la classe BandeauBas en appelant les setteurs appropriés.
     * @param array $pData : le tableau comportant toutes les informations
     * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
     * @version 1.0
     * @copyright Christophe Goidin - Mai 2013
     */
	public function __construct($pData) {
		// Hydratation de tous les attributs de la classe en appelant les mutateurs appropri�s
		foreach ($pData as $Cle => $Valeur) {
			$Methode = 'Set'.$Cle;
            if (is_callable(array($this, $Methode))) { // teste si la méthode $Methode est "appelable" dans la classe courante ($this) 
            	$this->$Methode($Valeur);
            }
		}
	}
 	
	// =====================================================================================================================================================
	// Les accesseurs (ou getter)
	// =====================================================================================================================================================
	/**
	 * Renvoie le copyright
	 * @param null 
	 * @return string : le copyright 
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	private function GetCopyright() {
    	return $this->Copyright;
	}
	
	
	// =====================================================================================================================================================
	// Les mutateurs (ou setter)
	// =====================================================================================================================================================
	/**
	 * Positionne le copyright
	 * @param string $pValue : le copyright
	 * @return null
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	private function SetCopyright($pValue) {
 		$this->Copyright = $pValue;
 	}
 	
	 	
 	// =====================================================================================================================================================
	// Les méthodes relatives à la génération du contenu XHTML
	// =====================================================================================================================================================
	/**
	 * Renvoie le bloc XHTML relatif au bandeau bas de la page Web
	 * @param null
	 * @return string : le bloc XHTML relatif au bandeau bas de la page Web
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */ 	
	public function GetXhtml() {
		$chaine = "<div id='Pied'>\n\t
					<div class='ColGauche'>
						<span>Les Films de la Cinémathèque</span>
						<ul><li><a href='./Index.php?Page=CinePassion38&amp;Action=Presentation'>Accueil</a></li>
							<li><a href='./Index.php?Page=CinePassion38&amp;Action=Partenaires'>Afficher la liste</a></li>
						</ul>
					</div>\n\t
			  		<div class='ColGauche'>
						<span class='Centrer'>Les Utilisateurs</span>
						<ul>";
					if (!isset($_SESSION['Infos']['Login'])){
						$chaine.="	<li><a href='./Index.php?Page=Utilisateur&amp;Action=Accueil'>Accueil</a></li>
									<li><a href='./Index.php?Page=Utilisateur&amp;Action=Inscription'>S'inscrire</a></li>";
					}else{
						$chaine.="	<li><a href='./Index.php?Page=Utilisateur&amp;Action=Accueil'>Accueil</a></li>
									<li><a href='./Index.php?Page=Utilisateur&amp;Action=ModifierInfos'>Modifier ses informations</a></li>
									<li><a href='./Index.php?Page=Utilisateur&amp;Action=ModifierMotDePasse'>Modifier son mot de passe</a></li>
									<li><a href='./Index.php?Page=Utilisateur&amp;Action=Deconnexion'>Déconnexion</a></li>
									<li><a href='./Index.php?Page=Utilisateur&amp;Action=SuppressionCompte'>Supprimer le compte</a></li>";
					}
		$chaine.="	</ul>
					</div>\n\t
					<div class='ColDroite'>A compléter</div>\n\t
					<div class='ColDroite'>A compléter</div>\n\t
					<div id='ColCentre'>
						<span class='Centrer'>L'association CinePassion38</span>
						<ul><li><a href='./Index.php?Page=CinePassion38&amp;Action=Presentation'>Présentation de notre association</a></li>
							<li><a href='./Index.php?Page=CinePassion38&amp;Action=Partenaires'>Nos partenaires</a></li>
							<li><a href='./Index.php?Page=CinePassion38&amp;Action=Plan'>Plan</a></li>
						</ul>
					</div>\n\t
					<hr/>\n
			  	</div>\n
			  	<div id='Certification'>
					<img alt='' src='".DIR_IMAGE_DIVERS."Pied.jpg' />
					<img alt='' src='".DIR_IMAGE_DIVERS."CinePassion38LogoMini.png' id='CinePassion38LogoMini' />
					<div id='W3C'>
						<img alt='logo_W3C_Xhtml_1.0' src='".DIR_IMAGE_DIVERS."W3C_Xhtml_1.0.png' />&nbsp;&nbsp;&nbsp;&nbsp;
						<img alt='logo_W3C_Css' src='".DIR_IMAGE_DIVERS."W3C_Css.png' />
					</div>
				</div>\n
				<div id='Copyright'>".$this->GetCopyright()."</div>";
					
		
		return $chaine;
	}
	
	/**
 	 * Méthode MAGIQUE appelée automatiquement lorsque l'utilisateur essaie d'afficher un objet de la classe. La méthode GetXhtml() est appelée.
 	 * @param null
 	 * @return string : le bloc XHTML relatif au bandeau bas de la page 
 	 * @author : Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 	 * @version : 1.0
 	 * @copyright Christophe Goidin - Mai 2013
 	 */
 	public function __toString() {
        return $this->GetXhtml();
    }

} // fin class


/**
 * La classe PageStandard permet de gérer la page standard du site
 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 * @version 1.0
 * @copyright Christophe Goidin - Mai 2013
 */
class PageStandard {
	
	// =====================================================================================================================================================
	// Les attributs
	// =====================================================================================================================================================
	private $BandeauHaut;
	private $BandeauBas;
	private $Page = array();	// tableau qui sera alimenté dynamiquement par les appels à la méthode magique __set
	private $RsaPublicKey;
	
	// =====================================================================================================================================================
	// Le constructeur
	// =====================================================================================================================================================
    /**
     * Le constructeur permet d'hydrater tous les attributs de la classe PageStandard en appelant les mutateurs appropriés
     * @param array $pData : le tableau comportant toutes les informations relatives au contenu de la page Web
     * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
     * @version 1.0
     * @copyright Christophe Goidin - Mai 2013
     */
	public function __construct($pData) {
 		if (!empty($pData)) {
 			foreach ($pData as $Cle => $Valeur) {
            	switch ($Cle) {
            		case "Page" : // Alimentation dynamique du tableau $Page en appelant implicitement la méthode magique __set pour chaque attribut inexistant
            			$this->TitreNavigateur = (!isset($pData['Page']['TitreNavigateur'])?"Inconnu":$pData['Page']['TitreNavigateur']);
            			$this->Doctype = (!isset($pData['Page']['Doctype'])?"XHTML 1.0 Strict":$pData['Page']['Doctype']);
            			$this->Titre = (!isset($pData['Page']['Titre'])?"Titre non défini":$pData['Page']['Titre']);
            			$this->ContenuSansEncarts = (!isset($pData['Page']['ContenuSansEncarts'])?"":$pData['Page']['ContenuSansEncarts']);
            			$this->ContenuAvecEncarts = (!isset($pData['Page']['ContenuAvecEncarts'])?"":$pData['Page']['ContenuAvecEncarts']);
            			$this->RsaPublicKey = (!isset($pData['Page']['RsaPublicKey'])?"":$pData['Page']['RsaPublicKey']);
            			break;
            		case "BandeauHaut" :
            			$this->BandeauHaut = new BandeauHaut($pData[$Cle]);
            			break;
               		case "BandeauBas" :
            			$this->BandeauBas = new BandeauBas($pData[$Cle]);
            			break;
               	}
            }
 		}
 	}
 	
 	
 	// =====================================================================================================================================================
	// Les accesseurs (ou getter)
	// =====================================================================================================================================================
	/**
	 * Méthode MAGIQUE permettant de retourner la valeur de l'attribut $pNomAttribut dans le tableau $Page 
	 * @param string $pNomAttribut : le nom de l'attribut
	 * @return string : la valeur correspondant à la clé $pNomAttribut dans le tableau $Page. Renvoie le booléen False si $pNomAttribut n'a pas été trouvé dans le tableau $Page 
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
 	public function __get($pNomAttribut) {
		if (array_key_exists($pNomAttribut, $this->Page)) {
            return $this->Page[$pNomAttribut];
        }else {
        	return false;
        }
    }

   	
   	// =====================================================================================================================================================
	// Les mutateurs (ou setter)
	// =====================================================================================================================================================
	/**
	 * Méthode MAGIQUE permettant d'alimenter le tableau $Page
	 * @param string $pNomAttribut : le nom de l'attribut
	 * @param string $pValeurAttribut : la valeur de l'attribut
	 * @return null
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
    public function __set($pNomAttribut, $pValeurAttribut) {
   	 	$this->Page[$pNomAttribut] = $pValeurAttribut;
	}
    
	
	// =====================================================================================================================================================
	// Les méthodes destinées à la génération de la page XTMLl
	// =====================================================================================================================================================
 	/**
	 * Renvoie le bloc XHTML relatif à la page Web
	 * @param null
	 * @return string : le bloc XHTML relatif à la page Web 
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 2.0
	 * @copyright Christophe Goidin - Novembre 2013
	 */
 	public function GetXhtmlPage() {
		// =====================================================================================================================================================
		// Génération du début de la page : doctype + ent�te + image de retour en haut de la page
		// =====================================================================================================================================================
		$Result = $this->GetXhtmlDoctype().
				  $this->GetXhtmlEntete().
				  "<body>\n
					   <div>
					   		<img alt = 'RetourHautDePage'
								 id = 'RetourHautDePage'
								 src = '".DIR_IMAGE_DIVERS."FlecheHaut.png'
								 onmouseover = \"window.document.getElementById('RetourHautDePage').src='".DIR_IMAGE_DIVERS."FlecheHautSurvol.png'\" 
								 onmouseout = \"window.document.getElementById('RetourHautDePage').src='".DIR_IMAGE_DIVERS."FlecheHaut.png'\"
								 onclick = \"$('html, body').animate({scrollTop:$('body').offset().top}, 'slow');\" />
					   </div>\n";
		
		// =====================================================================================================================================================
		// Bloquage du menu en haut de la page lors du défilement avec l'ascenseur vertical
		// Affichage dynamique de l'image permettant de revenir en haut de la page
		// =====================================================================================================================================================
		$Result .= "<script type='text/javascript'>
						$(function () {
    						$(window).scroll(function () {
       							var vPositionMenuGauche = window.document.getElementById('BandeauHaut').offsetLeft + 11; // +11 car il y a 10 pixels de marge interne gauche + 1pixel de bordure gauche
       							if ($(window).scrollTop() > 200) { 
         					 		$('div#Menu ul.Niveau1').css('position', 'fixed');
         					 		$('div#Menu ul.Niveau1').css('top', '0px');
         					 		$('div#Menu ul.Niveau1').css('left', vPositionMenuGauche + 'px');
         					 		$('img#RetourHautDePage').css('visibility', 'visible');
         					 	}else {
         					 		$('div#Menu ul.Niveau1').css('position', 'relative');
         					 		$('div#Menu ul.Niveau1').css('top', '0px');
         					 		$('div#Menu ul.Niveau1').css('left', '0px');
         					 		$('img#RetourHautDePage').css('visibility', 'hidden');
         					 	}
    						});
						});
					</script>";
		
		// Charger le script JS de la clé publique en fonction de la connexion
		if ($this->RsaPublicKey != ""){
					require_once(DIR_MODELE_PDO."class.Bd.inc.php");
					$Result .= "<script type='text/javascript'>
    								var vPublicKey = \"".$this->RsaPublicKey."\";
								</script>";
		}
		
		if(isset($_SESSION['Login'])){
			$Result .= "<script>setTimeout(function(){window.location='./Index.php?Page=Utilisateur&Action=Deconnexion';},300000)</script>";
		}
 		
		// =====================================================================================================================================================
		// Génération de la page : bandeau haut + menu + contenu + bandeau bas 
		// =====================================================================================================================================================
		/*$Result .= "<div id='ImageArrierePlan'><img alt='' src='./Image/Divers/Terre.png' /></div>".*/
		$Result .= 	$this->BandeauHaut->GetXhtml().
					$this->GetXhtmlMenu().
					$this->GetXhtmlContenu().
					$this->BandeauBas->GetXhtml().
					"</body>\n</html>";
		
		return $Result;
	}
	
	/**
	 * Renvoie le bloc XHTML relatif au doctype
	 * @param null
	 * @return string : le bloc XHTML relatif au doctype
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	private function GetXhtmlDoctype() {
		switch ($this->Doctype) { // Appel implicite à la méthode magique __get
			case "XHTML 1.0 Strict" :
				$Result = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n";
				break;
			case "XHTML 1.0 Transitional" :
				$Result = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n";
				break;
		}
		return $Result;
	}
	
	/**
	 * Renvoie le bloc Xhtml relatif à l'entête de la page Web (le bloc <head>), avec tous les liens nécessaires : feuilles de style CSS, scripts javascript, ...
	 * @param null
	 * @return string : le bloc XHTML relatif à l'entête de la page Web (le bloc <head>)
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Août 2013
	 */
	private function GetXhtmlEntete() {
		$Result = "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"fr\" lang=\"fr\">\n
				   <head>
					<link rel='icon' type='image/png' href='".DIR_IMAGE_DIVERS."Bobine.png' />
					<script type='text/javascript' src='".LIB_JQUERY."JQuery_v1.7.2_Prod.js'></script>
					<link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet'>
					<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n
					<title>".$this->TitreNavigateur."</title>\n"
					.$this->GetXhtmlEnteteLiens("StructurationBlocs.css")."\n"
					.$this->GetXhtmlEnteteLiens("Formulaire.css")."\n"
					.$this->GetXhtmlEnteteLiens("Menu.css")."\n"
					.$this->GetXhtmlEnteteLiens("MiseEnForme.css")."\n"
					.$this->GetXhtmlEnteteLiens("JQuery")."\n"
					.$this->GetXhtmlEnteteLiens("LiScroll")."\n"
					.$this->GetXhtmlEnteteLiens("SimpleSlideShow")."\n";
		
		if(!isset($_SESSION['Infos']['Login'])){
			$Result .= $this->GetXhtmlEnteteLiens("JSEncrypt")."\n";
			$Result .= $this->GetXhtmlEnteteLiens("ValidationFormulaire")."\n";
			$Result .= $this->GetXhtmlEnteteLiens("ValidationSaisie")."\n";
		}
							
		// =====================================================================================================================================================
		// Premier chargement du site en mode "normal"
		// =====================================================================================================================================================
		if (strlen($_SERVER['QUERY_STRING']) == 0) { 
			$ListeParam['Page'] = "Home";
			$ListeParam['Action'] = "AfficherAccueil";
			
		// =====================================================================================================================================================
		// Premier chargement du site en mode "débogage"
		// =====================================================================================================================================================
		}elseif (substr($_SERVER['QUERY_STRING'], 0, 20) == "XDEBUG_SESSION_START") { 
			$ListeParam['Page'] = "Home";
			$ListeParam['Action'] = "AfficherAccueil";
			
		// =====================================================================================================================================================
		// Cas général
		// =====================================================================================================================================================
		}else {
			foreach (explode("&", $_SERVER['QUERY_STRING']) as $Valeur) {
				$UnParam = explode("=", $Valeur);
				$ListeParam[$UnParam[0]] = $UnParam[1];
			}
		}
		
		switch($ListeParam['Page']) {
			// =====================================================================================================================================================
			// Accueil du site
			// =====================================================================================================================================================
			case "Home" :
				switch($ListeParam['Action']) {
					// =====================================================================================================================================================
					// Page d'accueil
					// =====================================================================================================================================================
					case "AfficherAccueil" :
						$Result .= $this->GetXhtmlEnteteLiens("FluxRss.css").
								   $this->GetXhtmlEnteteLiens("SlidesJS")."\n";
						break 2;
					// =====================================================================================================================================================
					// Page d'erreur
					// =====================================================================================================================================================
					default :
						break 2;
						
				} // switch niveau 2
				break; // inutile
				
			// =====================================================================================================================================================
			// Présentation de l'association CinePassion38
			// =====================================================================================================================================================
			case "CinePassion38" :
				switch($ListeParam['Action']) {
					// =====================================================================================================================================================
					// Notre association
					// =====================================================================================================================================================
					case "Presentation" :
						break 2;
					
					// =====================================================================================================================================================
					// Nos partenaires
					// =====================================================================================================================================================
					case "Partenaires" :
						break 2;
						
					// =====================================================================================================================================================
					// Plan
					// =====================================================================================================================================================
					case "Plan" :
						break 2;
					
					// =====================================================================================================================================================
					// Page d'erreur
					// =====================================================================================================================================================
					default :
						break 2;
				} // switch niveau 2
				break; // inutile
			
				// =====================================================================================================================================================
				// Présentation des films
				// =====================================================================================================================================================
				case "Film" :
					switch($ListeParam['Action']) {
						// =====================================================================================================================================================
						// Accueil
						// =====================================================================================================================================================
						case "Accueil" :
							$Result .= $this->GetXhtmlEnteteLiens("SimpleSlideShow");
							break 2;
								
							// =====================================================================================================================================================
							// Affichage des informations
							// =====================================================================================================================================================
						case "AfficherInformations" :
							break 2;
				
							// =====================================================================================================================================================
							// Affichage de la liste de films
							// =====================================================================================================================================================
						case "AfficherLaListe" :
							$Result .= $this->GetXhtmlEnteteLiens("Tableau.css")."\n";
							break 2;

                        // =====================================================================================================================================================
                        // Demandant quel film est à comparer
                        // =====================================================================================================================================================
						case "AfficherFilmsCommuns" :
							break 2;

                        // =====================================================================================================================================================
                        // Affichage des films en communs
                        // =====================================================================================================================================================
						case "AfficherResultatsFilmsCommuns" :
							break 2;

                        // =====================================================================================================================================================
						// Page d'erreur
						// =====================================================================================================================================================
					default :
						break 2;
					} // switch niveau 2
					break; // inutile
					
				// =====================================================================================================================================================
				// Utilisateur
				// =====================================================================================================================================================
				case "Utilisateur" :
					switch($ListeParam['Action']){
						// =====================================================================================================================================================
						// Accueil
						// =====================================================================================================================================================
						case "Accueil" :
							$Result .= $this->GetXhtmlEnteteLiens('')."\n";
							break 2;
							
							// =====================================================================================================================================================
							// Authentification
							// =====================================================================================================================================================
						case "Authentification" :
							$Result .= $this->GetXhtmlEnteteLiens("")."\n";
							break 2;
							
							// =====================================================================================================================================================
							// Inscription
							// =====================================================================================================================================================
						case "Inscription" :
							$Result .= $this->GetXhtmlEnteteLiens("PwdSecure")."\n";
							break 2;
							
							// =====================================================================================================================================================
							// ModifierInfos
							// =====================================================================================================================================================
						case "ModifierInfos" :
							$Result .= $this->GetXhtmlEnteteLiens("")."\n";
							break 2;
							
							// =====================================================================================================================================================
							// ModifierMotDePasse
							// =====================================================================================================================================================
						case "ModifierMotDePasse" :
							$Result .= $this->GetXhtmlEnteteLiens("PwdSecure")."\n";
							break 2;
							
							// =====================================================================================================================================================
							// Deconnexion
							// =====================================================================================================================================================
						case "Deconnexion" :
							$Result .= $this->GetXhtmlEnteteLiens("")."\n";
							break 2;
							
							// =====================================================================================================================================================
							// SuppressionCompte
							// =====================================================================================================================================================
						case "SuppressionCompte" :
							$Result .= $this->GetXhtmlEnteteLiens("")."\n";
							break 2;
					}
			// =====================================================================================================================================================
			// Page d'erreur
			// =====================================================================================================================================================
			default :
				break;
				
				
		} // switch niveau 1
		
		return $Result."</head>\n";
	} // function
	
	/**
	 * Renvoie la liste des liens à incorporer afin d'utiliser les fonctionnalités de la "librairie" ou du fichier dont le nom est passé en param�tre. Dans le cas d'une librairie, seul le nom est passé en paramètre. Dans le cas d'un fichier CSS, il faut préciser l'extension du fichier.
	 * @param string $pNom : le nom de la "librairie" ou le nom du fichier (avec son extension dans le cas d'une feuille de style CSS) dont on veut incorporer les liens
	 * @return string : la liste des liens à incorporer afin d'utiliser les fonctionnalit�s de la "librairie" ou du fichier dont le nom est passé en paramètre
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Septembre 2014
	 * @example : GetXhtmlEnteteLiens("LiScroll");    GetXhtmlEnteteLiens("Formulaire.css");
	 */
	private function GetXhtmlEnteteLiens($pNom) {
		// =====================================================================================================================================================
		// Cas d'une feuille de style CSS
		// =====================================================================================================================================================
	 	if (substr($pNom, strlen($pNom) - 4) == ".css") {
			$Chaine = "<link rel='stylesheet' type='text/css' href='".DIR_CSS.substr($pNom, 0, strlen($pNom) - 4).(CONF_MINIFY ? ".min.css" : ".css")."' />\n";
		
		// =====================================================================================================================================================
		// Cas d'un fichier javascript ou d'une librairie
		// =====================================================================================================================================================
	  	}else {
			switch($pNom) {
				case "JQuery" :
					$Chaine = "<script type='text/javascript' src='".LIB_JQUERY."JQuery_v1.7.2_Prod.js'></script>";
					break;
				case "LiScroll" :
					$Chaine = "<link rel='stylesheet' type='text/css' href='".LIB_LISCROLL."LiScroll1.0.css' />\n
							   <script type='text/javascript' src='".LIB_LISCROLL."LiScroll1.0.js'></script>";
					break;
				case "SlidesJS" :
					$Chaine = "<link rel='stylesheet' type='text/css' href='".LIB_SLIDESJS."css/global.css' />\n
							   <script type='text/javascript' src='".LIB_SLIDESJS."js".DIRECTORY_SEPARATOR."slides.min.jquery.js'></script>";
					break;
				case "SimpleSlideShow" :
					$Chaine = "<link rel='stylesheet' type='text/css' href='".LIB_SIMPLESLIDESHOW."css/SimpleSlideShow.css' />\n
								<script type='text/javascript' src='".LIB_SIMPLESLIDESHOW."js/SimpleSlideShow.js'></script>";
					break;
				case "LightBox2" :
					$Chaine = "<script type='text/javascript' src='".LIB_LIGHTBOX2."js/lightbox.js'></script>";
					//src='".LIB_LIGHTBOX2."js/lightbox.js'
					break;
				case "JSEncrypt" :
					$Chaine = "<script type='text/javascript' src='".LIB_JSENCRYPT."jsencrypt.js'></script>";
					break;
				case "PHPSecLib" :
					$Chaine = "<script type='text/javascript' src='".LIB_PHPSECLIB."'></script>";
					break;
				case "FormulaireCSS" :
					$Chaine = "<link rel='stylesheet' type='text/css' href='".DIR_CSS."Formulaire.css'/>";
					break;
				case "ValidationFormulaire" :
					$Chaine = "<script type='text/javascript' src='".DIR_JS."ValidationFormulaire.js' ></script>";
					break;
				case "ValidationSaisie" :
					$Chaine = "<script type='text/javascript' src='".DIR_JS."ValidationSaisie.js' ></script>";
					break;
                case "PwdSecure" :
                    $Chaine = "<link rel='stylesheet' type='text/css' href='".LIB_PWDSECURE."PwdSecure.css' />\n
								<script type='text/javascript' src='".LIB_PWDSECURE."PwdSecure.js'></script>";
                    break;
					
				default :
					$Chaine = "";
					break;
			}	
		}
		return $Chaine;
	}
	
	/**
	 * Renvoie le bloc XHTML relatif au menu de la page Web
	 * @param null
	 * @return string : le bloc XHTML relatif au menu de la page Web
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	
	private function GetXhtmlMenu() {
		$chaine = "<div id='Menu'>
					<ul class='Niveau1'>
					<li id='Home'><a href='./Index.php?Page=Home&amp;Action=AfficherAccueil'>&nbsp;</a></li>
					<li class='Plus'><a href='./Index.php?Page=Film&amp;Action=Accueil'>Film</a>
						<ul class='Niveau2'>
							<li><a href='./Index.php?Page=Film&amp;Action=AfficherLaListe'>Afficher la liste</a></li>
							<li><a href='./Index.php?Page=Film&amp;Action=AfficherFilmsCommuns'>Films en communs</a></li>
						</ul>
					</li>
					<li class='Plus'><a href='./Index.php?Page=Utilisateur&amp;Action=Accueil'>Utilisateur</a>
						<ul class='Niveau2'>\n";
		if (!isset($_SESSION['Login'])){
			$chaine .="<li><a href='./Index.php?Page=Utilisateur&amp;Action=Inscription'>S'inscrire</a></li>";
				
		}else{
			$chaine .="	<li class='Plus'><a href='./Index.php?Page=Utilisateur&amp;Action=ModifierInfos'>Modifications</a>
									<ul class='Niveau3'>
										<li><a href='./Index.php?Page=Utilisateur&amp;Action=ModifierInfos'>Informations</a></li>
										<li><a href='./Index.php?Page=Utilisateur&amp;Action=ModifierMotDePasse'>Mot de passe</a></li>
									</ul>
						</li>
							<li><a href='./Index.php?Page=Utilisateur&amp;Action=Deconnexion'>Déconnexion</a></li>
							<li><a href='./Index.php?Page=Utilisateur&amp;Action=SuppressionCompte'>Supprimer son compte</a></li>\n";
		}
		$chaine .= "	</li>
					</ul>
				</li>
			</ul>
		</div>\n";
		return $chaine;
	}
	
	/**
	 * Renvoie le bloc XHTML relatif au contenu de la page Web. Une marge est automatiquement positionnée si les 2 blocs Contenu sont renseignés (ContenuSansEncarte et ContenuAvecEncarts)
	 * @param null
	 * @return string : le bloc XHTML relatif au contenu de la page Web
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	protected function GetXhtmlContenu() {
		return "<div id='Contenu'>
					<span id='TitrePage'>".$this->Titre."</span>
					<div id='BlocSansEncarts'>".
						 $this->ContenuSansEncarts.
						 ((strlen($this->ContenuSansEncarts) != 0 && strlen($this->ContenuAvecEncarts) != 0)?"<hr class='Marge' />":"").
						 $this->ContenuAvecEncarts."
					</div>
					<hr/>
				</div>\n";
	}
	
	/**
	 * 
	 * @param 
	 * @return 
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin
	 */
	private function GetAuthentification(){
		if (!isset($_SESSION['Infos']['Login'])){
			
		}
	}
 	
} // fin class


/**
 * La classe PageAvecEncarts permet de gérer les pages comprenant des encarts
 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 * @version 1.0
 * @copyright Christophe Goidin - Mai 2013
 */
class PageAvecEncarts extends PageStandard {
	
	// =====================================================================================================================================================
	// Les attributs
	// =====================================================================================================================================================
 	private $LesEncarts1;	// Première collection d'encarts
	private $LesEncarts2; 	// Seconde collection d'encarts
	
	
	// =====================================================================================================================================================
	// Le constructeur
	// =====================================================================================================================================================
 	/**
	 * Le constructeur permet d'appeler le constructeur de la classe mère et aussi d'hydrater les collections d'encarts "LesEncarts1" et "LesEncarts2". 
	 * @param array $pData : le tableau comportant toutes les informations relatives au contenu de la page Web
	 * @return null
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	public function __construct($pData) {
 		parent::__construct($pData);
 		$this->LesEncarts1 = new collection();
 		$this->LesEncarts2 = new collection();
		if (array_key_exists("Encart", $pData)) {
			if (array_key_exists("1", $pData['Encart'])) {
 				foreach ($pData['Encart']['1'] as $UnEncart) {
 					$this->LesEncarts1->Add(new Encart($UnEncart));
 				}
			}
			if (array_key_exists("2", $pData['Encart'])) {
				foreach ($pData['Encart']['2'] as $UnEncart) {
 					$this->LesEncarts2->Add(new Encart($UnEncart));
 				}
			}
 		}
 	}

 	
 	// =====================================================================================================================================================
	// Les méthodes destinées à la génération de la page XHTML
	// =====================================================================================================================================================
 	/**
	 * Renvoie la liste des encarts
	 * @param integer $pPosition : la position des encarts à récupérer : 1, 2 ou null (les deux) 
	 * @return string : le bloc XHTML relatif aux encarts
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
 	protected function GetXhtmlLesEncarts($pPosition = null) {
  		switch($pPosition) {
 			case 1 :
 				return $this->LesEncarts1->GetListeElements();
 				break;
 			case 2 :
 				return $this->LesEncarts2->GetListeElements();
 				break;
 			default :
 				return $this->LesEncarts1->GetListeElements().$this->LesEncarts2->GetListeElements();
 				break;
 		}
 	}
  	
} // fin class


/**
 * La classe PageAvecEncartGauche permet de gérer la page comprenant des encarts positionnés à gauche du contenu
 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 * @version 1.0
 * @copyright Christophe Goidin - Mai 2013
 */
 class PageAvecEncartsGauche extends PageAvecEncarts {

 	// =====================================================================================================================================================
	// Les attributs
	// =====================================================================================================================================================
 	// null
 	
	// =====================================================================================================================================================
	// Le constructeur
	// =====================================================================================================================================================
 	/**
	 * Le constructeur permet d'appeler le constructeur de la classe mère 
	 * @param array $pData : le tableau comportant toutes les informations relatives au contenu de la page Web
	 * @return null
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
 	public function __construct($pData) {
 		parent::__construct($pData);
	}
	
	
	// =====================================================================================================================================================
	// Les méthodes destinées à la génération de la page XHTML
	// =====================================================================================================================================================
 	/**
	 * Renvoie le bloc XHTML relatif au contenu de la page Web avec les encarts positionnés à gauche
	 * @param null
	 * @return string : le bloc XHTML relatif au contenu de la page Web
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	protected function GetXhtmlContenu() {
		// La référence aux propriétés (titre, ...) appelle implicitement la méthode magique __get 
		return "<div id='Contenu'>
				<span id='TitrePage'>".$this->Titre."</span>
				<div id='BlocSansEncarts'>".$this->ContenuSansEncarts."</div>
				<hr class='Marge' />
				<div id='BlocGauche'>".$this->GetXhtmlLesEncarts()."</div> 
				<div id='BlocCentre'>".$this->ContenuAvecEncarts."</div>
		  		<hr/>
		  	</div>\n";
	}
	
} // fin class

/**
 * La classe PageAvecEncartDroite permet de gérer la page comprenant des encarts positionnés à droite du contenu
 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 * @version 1.0
 * @copyright Christophe Goidin - Mai 2013
 */
 class PageAvecEncartsDroite extends PageAvecEncarts {
	
	// =====================================================================================================================================================
	// Les attributs
	// =====================================================================================================================================================
 	// null
 	
	// =====================================================================================================================================================
	// Le constructeur
	// =====================================================================================================================================================
 	/**
	 * Le constructeur permet d'appeler le constructeur de la classe mère. 
	 * @param array $pData : le tableau comportant toutes les informations relatives au contenu de la page Web
	 * @return null
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
 	public function __construct($pData) {
 		parent::__construct($pData);
	}
	
 	
 	// =====================================================================================================================================================
	// Les méthodes destinées à la génération de la page XHTMLl
	// =====================================================================================================================================================
 	/**
	 * Renvoie le bloc XHTML relatif au contenu de la page Web avec les encarts positionnés à droite
	 * @param null
	 * @return string : le bloc XHTML relatif au contenu de la page Web
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	protected function GetXhtmlContenu() {
		return "<div id='Contenu'>
				<span id='TitrePage'>".$this->Titre."</span>
				<div id='BlocSansEncarts'>".$this->ContenuSansEncarts."</div>
				<hr class='Marge' />
				<div id='BlocDroite'>".$this->GetXhtmlLesEncarts()."</div> 
				<div id='BlocCentre'>".$this->ContenuAvecEncarts."</div>
				<hr/>
		  	</div>\n";
	}
	
} // fin class


/**
 * La classe PageAvecEncartsGaucheDroite permet de gérer la page comprenant des encarts positionnés à gauche ET à droite du contenu
 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 * @version 1.0
 * @copyright Christophe Goidin - Mai 2013
 */
 class PageAvecEncartsGaucheDroite extends PageAvecEncarts {
	
	// =====================================================================================================================================================
	// Les attributs
	// =====================================================================================================================================================
 	// null
 	
	// =====================================================================================================================================================
	// Le constructeur
	// =====================================================================================================================================================
 	/**
	 * Le constructeur permet d'appeler le constructeur de la classe mère 
	 * @param array $pData : le tableau comportant toutes les informations relatives au contenu de la page Web
	 * @return null
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
 	public function __construct($pData) {
 		parent::__construct($pData);
	}
	
 	
 	// =====================================================================================================================================================
	// Les méthodes destinées à la génération de la page XHTML
	// =====================================================================================================================================================
 	/**
	 * Renvoie le bloc XHTML relatif au contenu de la page Web avec des encarts positionnés à gauche ET à droite
	 * @param null
	 * @return string : le bloc XHTML relatif au contenu de la page Web
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	protected function GetXhtmlContenu() {
		return "<div id='Contenu'>
				<span id='TitrePage'>".$this->Titre."</span>
				<div id='BlocSansEncarts'>".$this->ContenuSansEncarts."</div>
				<hr class='Marge' />
				<div id='BlocGauche'>".$this->GetXhtmlLesEncarts(1)."</div>
				<div id='BlocDroite'>".$this->GetXhtmlLesEncarts(2)."</div> 
				<div id='BlocCentre'>".$this->ContenuAvecEncarts."</div>
		  		<hr/>
		  	</div>\n";
	}

} // fin class

?>
