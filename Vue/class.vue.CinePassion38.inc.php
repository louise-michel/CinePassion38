<?php
/*=============================================================================================================
	Fichier				: class.vue.CinePassion38.inc.php
	Auteur				: Christophe Goidin (christophe.goidin@ac-grenoble.fr)
	Date de création	: Août 2013
	Date de modification: 
	Rôle				: Gère les vues partielles du module permettant de présenter l'association CinePassion38, les partenaires et le plan
===============================================================================================================*/

/**
 * Classe relative à la vue de l'association CinePassion38
 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 * @version 1.0
 * @copyright Christophe Goidin - Août 2013
 */
class VueCinePassion38 {
	// =====================================================================================================================================================
	// Les attributs
	// =====================================================================================================================================================
 	// null
	    
	    
	// =====================================================================================================================================================
	// Les méthodes destinées à la génération du contenu XHTML
	// =====================================================================================================================================================
	public static function GetXhtmlContenu($pTab) {
    	$Result = "";
		foreach ($pTab as $Cle => $UneRubrique) {
    		$Result .= "<span class='RubriqueTitre'>".$UneRubrique['Titre']."</span>";
    		$Result .= "<span class='RubriqueInfos'>".$UneRubrique['Infos'];
    		if ($Cle == "Organigramme") {
    			if (isset($UneRubrique['Image'])) {
    				$Result .= "<span class='Centrer'>".$UneRubrique['Image']."</span>";
    			}
    		}elseif ($Cle == "Plan") {
    			if (isset($UneRubrique['Image'])) {
    				$Result .= "<br/><br/><span class='Centrer'>".$UneRubrique['Image']."</span>";
    			}
    		}elseif (($Cle == "Partenaire1") OR ($Cle == "Partenaire2") OR ($Cle == "Partenaire3") OR ($Cle == "Partenaire4")) {
    			if (isset($UneRubrique['Logo'])) {
    				$Result .= "<br/><br/><span class='Centrer'>".$UneRubrique['Logo']."</span>";
    			}
    		} 
    		$Result .= "</span>";
    	}
		return $Result;
	}	
	
} // fin class

?>