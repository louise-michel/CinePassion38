# Projet CinePassion38 Groupe 07
> Vitti Clément, Blanc-Coquand Etienne

## Fonctionnement des commits avec Git
Git add -> Git Commit -> Git Push -> Envoi sur GitHub

## Vue d'ensemble
L'association CinePassion38 veut proposer un site pour avoir une meilleure visibilité sur internet auprès du plus grand nombre de personnes.

Ce site doit pouvoir proposer un catalogue pour que les utilisateurs puissent consulter les informations des films, celui-ci doit aussi être un outil de communication pour permettre aux membres de l’associations d’échanger entre eux sur les films et pouvoir les noter facilement.

## Commandes GIT utiles
Avant-tout, installer [Git](https://git-scm.com).

`git init` = Initialiser le dossier parent (déjà fait si récupéré de GitHub).

`git clone https://github.com/Bl0uf/CinePassion38.git` = Cloner le serveur distant sur la version locale.

`git fetch [nomDistant]` = Cette commande s'adresse au dépôt distant et récupère toutes les données de ce projet que vous ne possédez pas déjà. Après cette action, vous possédez toutes les références à toutes les branches contenues dans ce dépôt, que vous pouvez fusionner ou inspecter à tout moment.

`git status` = Voir l'état des fichiers (_vert_ = prêt à commit (donc un `git add` a déjà été fait) et _rouge_ = non prêt à commit (donc faire un `git add`)).

`git add nomFichier` = Ajouter un fichier pour le préparer à commit.

`git commit nomFichier -m "Commentaire"` = Permet de commit un fichier avec un commentaire **ATTENTION**, le fichier n'est pas encore envoyé sur GitHub.

`git push -u origin Branch` = Permet d'envoyer les modifications commité vers le site GitHub (`Branch` a remplacer par le nom de la branche, par exemple le master remplace `Branch` par `master`).

### _Entraînement BC_
> _Ajouter une page qui affiche une liste déroulante où il faut choisir un film, les films sont triés par ordre alphabétique dans la liste, ce qui va rediriger vers une autre page, après une action de l'utilisateur,  pour afficher les films du même genre._

**_Entraînement supplémentaire_**
> _Ajouter une autre liste déroulante où l'utilisateur choisit de voir des films qui ont le même genre, le même réalisateur, la même année de sortie, ..._

### _Entraînement VITTI_
> _A toi de mettre ce que tu veux pour le lot 02_