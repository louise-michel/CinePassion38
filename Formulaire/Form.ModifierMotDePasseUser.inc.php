<?php ?>
<div class="FormModifMdp">
	<form id="FormModifMDPUser" action="./Index.php?Page=Utilisateur&amp;Action=ModifierMotDePasse">
        <label class="labelPswd" id="labelOldPswd">Ancien mot de passe :</label>
		<input type="password" name="AncienMDP" id="AncienMDP" class="champMdp" maxlength="20" onkeypress='return VerificationSaisieModifPswd(window.event.which);'>
		<br/>

        <label class="labelPswd" id="labelNewPswd">Nouveau mot de passe :</label>
		<input type="password" name="NouveauMDP" id="NouveauMDP" class="champMdp" maxlength="20" onkeypress='return VerificationSaisieModifPswd(window.event.which);' onkeyup="TesterNiveauSecuriteMotDePasse(this.value);">
        <br/>

        <label class="labelPswd" id="labelConfPswd">Confirmation mot de passe :</label>
		<input type="password" name="ResaisirMDP" id="ResaisirMDP" maxlength="20" onkeypress='return VerificationSaisieModifPswd(window.event.which);' onkeyup="TesterNiveauSecuriteMotDePasse(this.value); TesterEgaliteMotDePasse(window.document.getElementById('NouveauMDP').value; window.document.getElementById('ResaisirMDP').value">
      	<br/>
        
        <div id="ErreurSaisieModifPswd"></div>
		<input type="submit" value="Changer">
		<input type="reset" value="Effacer">
	</form>

    <div id="pwdSecure">
        <span id="niveauSecurite">
            <span id="CouleurNiveauSecurite">

            </span>
        </span>
        <div id="barreSecurite">
            <span id="voyantSecurite">
            </span>
        </div>
        <span id="testEgalite">
            <!--IMG-->
            <!--TEXTE-->
        </span>
    </div>
</div>