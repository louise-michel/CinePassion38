<?php
	session_start();
	require_once('./Include/Config.inc.php');
	require_once(DIR_VUE.'class.vue.Pages.inc.php');
	require_once(DIR_CLASS.'class.Collection.inc.php');
	require_once(DIR_CLASS.'class.Encart.inc.php');
	
	$Page = new PageAvecEncartsDroite($_SESSION['Infos']);
	echo $Page->GetXhtmlPage();
?>