<?php
/*=============================================================================================================
	Fichier				: ctrl.AfficherAccueil.inc.php (Back contrôleur)
	Auteur				: Christophe Goidin (christophe.goidin@ac-grenoble.fr)
	Date de création	: Juillet 2012
	Date de modification: Mai 2013 		-> Prise en compte de la programmation orientée objet
						  Juillet 2013	-> Certaines initialisations sont réalisées dans le contrôleur principal
						  Août 2013		-> Appel de la vue partielle pour gérer la galerie photos 
	Rôle				: La page d'accueil du site
===============================================================================================================*/


// =====================================================================================================================================================
// Paramétrage de la page web
// =====================================================================================================================================================
/* Si la variable $TexteDefilant est renseignée avec : 
	- ""									=> RIEN ne défilera dans l'entête de la page
	- "Defaut"								=> Le contenu du fichier texte par défaut relatif au module défilera dans l'entête de la page web
	- le nom d'un fichier texte EXISTANT	=> Le contenu de ce fichier texte défilera dans l'entête de la page web
	- le nom d'un fichier texte INEXISTANT	=> RIEN ne défilera dans l'entête de la page
	*/  
$TexteDefilant = "DerniersFilmsAjoutes";

// =====================================================================================================================================================
// Bandeau haut
// =====================================================================================================================================================
$Infos['BandeauHaut']['Titre'] = "Présentation du module film";

if ($TexteDefilant == "") {
	// On ne renseigne pas la variable : $Infos['BandeauHaut']['TexteDefilant']
}elseif ($TexteDefilant == "Defaut") {
	$Infos['BandeauHaut']['TexteDefilant'] = DIR_TEXTE_DEFILANT."Module".fNomDossierParent(__FILE__).".txt";
}elseif (is_file(DIR_TEXTE_DEFILANT.$TexteDefilant.".txt")) {
	$Infos['BandeauHaut']['TexteDefilant'] = DIR_TEXTE_DEFILANT.$TexteDefilant.".txt";
}else {
	// On ne renseigne pas la variable : $Infos['BandeauHaut']['TexteDefilant']
}


// =====================================================================================================================================================
// Bandeau bas
// =====================================================================================================================================================
// Null


// =====================================================================================================================================================
// Les encarts
// =====================================================================================================================================================
/*  Chaque encart est incorpor� sous la forme : $Infos['Encart'][x][] = <adresse fichier encart> (x peut prendre les valeurs 1 ou 2)
 *  si page = PageStandard					-> les encarts sont ignorés
 *  si page = PageAvecEncartsDroite			-> tous les encarts (1 et 2) sont positionnés à la suite dans la partie droite de la page
 *  si page = PageAvecEncartsGauche			-> tous les encarts (1 et 2) sont positionnés à la suite dans la partie gauche de la page
 *  si page = PageAvecEncartsGaucheDroite	-> les encarts (1) sont positionnés dans la partie gauche de la page
 *  										   les encarts (2) sont positionnés dans la partie droite de la page
 */ 

$Infos['Encart'][1][] = DIR_ENCART."AvantagesInscription.txt";
$Infos['Encart'][2][] = DIR_ENCART."AvantagesInscription.txt";


// =====================================================================================================================================================
// Informations générales sur la page
// =====================================================================================================================================================
//$Infos['Page']['Doctype'] = "XHTML 1.0 Transitional";

// =====================================================================================================================================================
// Inclusion des fichiers nécessaires
// =====================================================================================================================================================
require_once(fGetVue(__FILE__));
require_once(DIR_MODELE_PDO."class.Bd.inc.php");
$Infos['BandeauHaut']['Titre'] = "Présentation du module film";
// =====================================================================================================================================================
// Préparation des données à fournir à la vue : Le tableau $Vue
// =====================================================================================================================================================
$NbImages = fGetNbFichiers(DIR_FILM_AFFICHE, "jpg");
for($i=0; $i<=NB_FILM_SLIDESHOW-1; $i++){
	$UneAffiche=fGetUneImage(DIR_FILM_AFFICHE, "jpg", mt_rand(0, $NbImages-1));
	$Vue['Galerie'][$i]=utf8_encode($UneAffiche);
}

$Infos['Page']['Titre'] = "Présentation du module film";
$Vue['TexteAccueil'] = "Le module \"Film\" permet d'avoir accès à l'intégralité des fonctionnalités de notre cinémathèque en rapport avec les films. Ces fonctionnalités sont toutes accessibles à partir du menu \"film\" ou directement à partir de certaines pages. Notre cinémathèque est actuellement composée de ".Bd::GetNbFilms()." films répartis dans des genres très variés afin de plaire au plus grand nombre : drame, comédie romantique, horreur, fantastique, animation, ... <br/><br/>
Le module \"Film\" permet de découvrir l'intégralité des films présents dans notre cinémathéque. Plusieurs fonctionnalités sont accessibles. Vous avez notamment la possibilité de visualiser la <a class='Souligne' href='./Index.php?Page=Film&amp;Action=AfficherLaListe' >liste intégrale des films</a> triés par ordre croissant. Ces films vous sont présentés sous forme d'un tableau incluant un système de navigation afin de permettre un déplacement aisé de page en page.<br/><br/> 
Une autre page vous présente les informations détaillées de chaque film. De nombreuses informations sont alors présentées sous forme d'onglets afin d'assurer aux visiteurs une navigabilité optimale. Parmi les informations accessibles, on peut citer entre autre : le résumé d'un film, sa date de sortie, sa durée, le nom du réalisateur et des acteurs avec leur rôle respectif, le genre du film, ... Deux possibilités s'offrent à vous afin de visualiser les informations détaillées d'un film : soit en cliquant sur la jaquette d'un film de cette page parmi celles qui défilent aléatoirement, soit en cliquant sur un film à partir de la liste des films.<br/><br/> 
Les membres de l'association sont les seuls à avoir la possibilité d'évaluer facilement et de commenter chacun des films présents. Les \"simples\" visiteurs, quant à eux, ne pourront que visualiser ces informations dispensées par nos différents membres.<br/><br/>
Ci-dessus, un aperçu de quelque uns des films présents dans notre cinémathèque. Un simple clic sur une des jaquettes d'un film permet d'accéder à sa fiche descriptive complète.";

// =====================================================================================================================================================
// Informations concernant la partie centrale de la page + appel de la vue partielle afin de gérer son affichage
// =====================================================================================================================================================
$Infos['Page']['ContenuAvecEncarts'] = VueFilm::GetXhtmlSlideShow($Vue['Galerie']).VueFilm::GetXhtmlCinePassion38($Vue);


// =====================================================================================================================================================
// Création d'une page en lui passant en paramètre le tableau $Infos. Si ce n'est pas fait, une page "standard" sera créée par défaut dans le contrôleur principal
// =====================================================================================================================================================
$Page = new PageAvecEncartsDroite($Infos);

?>
