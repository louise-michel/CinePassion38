<?php
/*=============================================================================================================
	Fichier				: ctrl.AfficherAccueil.inc.php (Back contrôleur)
	Auteur				: Christophe Goidin (christophe.goidin@ac-grenoble.fr)
	Date de création	: Juillet 2012
	Date de modification: Mai 2013 		-> Prise en compte de la programmation orientée objet
						  Juillet 2013	-> Certaines initialisations sont réalisées dans le contrôleur principal
						  Août 2013		-> Appel de la vue partielle pour gérer la galerie photos 
	Rôle				: La page d'accueil du site
===============================================================================================================*/

// =====================================================================================================================================================
// Inclusion des fichiers nécessaires
// =====================================================================================================================================================

require_once(fGetVue(__FILE__));
require_once(DIR_CLASS."class.Navigation.inc.php");
require_once(DIR_MODELE_PDO."class.Bd.inc.php");

// =====================================================================================================================================================
// Paramétrage de l'URL
// =====================================================================================================================================================
/* Si la valeur du paramètre dans l'URL n'existe pas alors le numéro de la page sera fixé à 1
 * Si la valeur du paramètre dans l'URL n'est pas une valeur numérique alors le numéro de la page sera fixé à 1
 * Si la valeur du paramètre dans l'URL est strictement inférieur à 1 alors le numéro de la page sera fixé à 1
 * Si la valeur du paramètre dans l'URL est strictement supérieur au nombre total de pages, alors le numéro de la page sera fixé au numéro total de pages
 * Dans les autres cas, la valeur du paramètre sera récupéré de l'URL
 */
$NbFilms = Bd::GetNbFilms();
$NbPages = fGetNbPages($NbFilms);

// ===============================================
// NumPage
// ===============================================
if (!isset($_GET['NumPage'])){
	$URL['NumPage'] = 1;
}elseif (!is_numeric($_GET['NumPage'])){
	$URL['NumPage'] = 1;
}elseif ($_GET['NumPage'] < 1){
	$URL['NumPage'] = 1;
}elseif ($_GET['NumPage'] > $NbPages){
	$URL['NumPage'] = $NbPages;
}else {
	$URL['NumPage'] = $_GET['NumPage'];
}

// ===============================================
// NumFilm
// ===============================================
if (!isset($_GET['NumFilm'])){
	$URL['NumFilm'] = 1;
}elseif (!is_numeric($_GET['NumFilm'])){
	$URL['NumFilm'] = 1;
}elseif ($_GET['NumFilm'] < 1){
	$URL['NumFilm'] = 1;
}elseif ($_GET['NumFilm'] > $NbFilms){
	$URL['NumFilm'] = $NbFilms;
}else {
	$URL['NumFilm'] = $_GET['NumFilm'];
}

// ===============================================
// Onglet
// ===============================================
if (!isset($_GET['Onglet'])){
	$URL['Onglet'] = "Informations";
}elseif (!is_string($_GET['Onglet'])){
	$URL['Onglet'] = "Informations";
}elseif ($_GET['Onglet'] !="Informations" || "Histoire" || "Acteurs" || "Notation" || "Commentaires"){
	$URL['Onglet'] = "Informations";
}else{
	$URL['Onglet'] = $_GET['Onglet'];
}

// =====================================================================================================================================================
// Paramétrage de la page web
// =====================================================================================================================================================
/* Si la variable $TexteDefilant est renseignée avec : 
	- ""									=> RIEN ne défilera dans l'entête de la page
	- "Defaut"								=> Le contenu du fichier texte par défaut relatif au module défilera dans l'entête de la page web
	- le nom d'un fichier texte EXISTANT	=> Le contenu de ce fichier texte défilera dans l'entête de la page web
	- le nom d'un fichier texte INEXISTANT	=> RIEN ne défilera dans l'entête de la page
	*/  
$TexteDefilant = "DerniereMinute";

// =====================================================================================================================================================
// Bandeau haut
// =====================================================================================================================================================
$Infos['BandeauHaut']['Titre'] = "Liste des films";

if ($TexteDefilant == "") {
	// On ne renseigne pas la variable : $Infos['BandeauHaut']['TexteDefilant']
}elseif ($TexteDefilant == "Defaut") {
	$Infos['BandeauHaut']['TexteDefilant'] = DIR_TEXTE_DEFILANT."Module".fNomDossierParent(__FILE__).".txt";
}elseif (is_file(DIR_TEXTE_DEFILANT.$TexteDefilant.".txt")) {
	$Infos['BandeauHaut']['TexteDefilant'] = DIR_TEXTE_DEFILANT.$TexteDefilant.".txt";
}else {
	// On ne renseigne pas la variable : $Infos['BandeauHaut']['TexteDefilant']
}


// =====================================================================================================================================================
// Bandeau bas
// =====================================================================================================================================================
// Null

// =====================================================================================================================================================
// Préparation des données à fournir à la vue : Le tableau $Vue
// =====================================================================================================================================================

$Vue['LeFilm'] = Bd::GetDetailFilm($URL['NumFilm']);
$Vue['Onglet'] = $URL['Onglet'];
$Vue['NumPage'] = $URL['NumPage'];
//$Vue['FilmsRealisateur'] = Bd::GetListeFilmsParRealisateur();
//$Vue['PositionFilmRealisateur'] = Bd::GetPositionFilmParRealisateur();

$Vue['Informations'] = $Vue['LeFilm']['TitreFilm']." est le 'Xème' film dans notre cinémathèque du réalisateur '".$Vue['LeFilm']['LibellePays']."' ".$Vue['LeFilm']['PrenomPersonne']." ".$Vue['LeFilm']['NomPersonne']."."
						." C'est un film 'PaysFilm' du genre ".$Vue['LeFilm']['LibelleGenre']." d'une durée de '".$Vue['LeFilm']['DureeFilm']."' qui est sorti en France le '".$Vue['LeFilm']['DateSortieFilm']."' dans les salles de cinéma.";
$Vue['Histoire'] = "Histoire : ".$Vue['LeFilm']['SynopsisFilm'];
$Vue['Acteurs'] = Bd::GetListeActeurs($URL['NumFilm']);

// =====================================================================================================================================================
// Les encarts
// =====================================================================================================================================================
/*  Chaque encart est incorpor� sous la forme : $Infos['Encart'][x][] = <adresse fichier encart> (x peut prendre les valeurs 1 ou 2)
 *  si page = PageStandard					-> les encarts sont ignorés
 *  si page = PageAvecEncartsDroite			-> tous les encarts (1 et 2) sont positionnés à la suite dans la partie droite de la page
 *  si page = PageAvecEncartsGauche			-> tous les encarts (1 et 2) sont positionnés à la suite dans la partie gauche de la page
 *  si page = PageAvecEncartsGaucheDroite	-> les encarts (1) sont positionnés dans la partie gauche de la page
 *  										   les encarts (2) sont positionnés dans la partie droite de la page
 */ 

$Infos['Encart'][1][] = DIR_ENCART."NosPartenaires.txt";
$Infos['Encart'][2][] = DIR_ENCART."News.txt";

// =====================================================================================================================================================
// Informations générales sur la page
// =====================================================================================================================================================
//$Infos['Page']['Doctype'] = "XHTML 1.0 Transitional";
$Infos['BandeauHaut']['Titre'] = $Vue['LeFilm']['TitreFilm'];

// =====================================================================================================================================================
// Informations concernant la partie centrale de la page + appel de la vue partielle afin de gérer son affichage
// =====================================================================================================================================================
$Infos['Page']['Titre'] = "Information détaillées concernant le film : ".$Vue['LeFilm']['TitreFilm'];
$Infos['Page']['ContenuSansEncarts'] = "";
//$Infos['Page']['ContenuAvecEncarts'] = VueFilm::GetXhtmlTableauListeFilms($Vue).$BarreNavigation->GetXhtmlBoutons().$BarreNavigation->GetXhtmlNumeros();
$Infos['Page']['ContenuAvecEncarts'] = VueFilm::GetImageFilm($Vue["LeFilm"]["TitreFilm"]).VueFilm::GetTexteOnglet($Vue);

// =====================================================================================================================================================
// Création d'une page en lui passant en paramètre le tableau $Infos. Si ce n'est pas fait, une page "standard" sera créée par défaut dans le contrôleur principal
// =====================================================================================================================================================
$Page = new PageAvecEncartsDroite($Infos);