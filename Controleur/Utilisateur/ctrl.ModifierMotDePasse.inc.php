<?php
/*=============================================================================================================
	Fichier				: ctrl.ModifierMotDePasse.inc.php (Back contrôleur)
	Auteur				: BC Etienne, Vitti Clément
	Date de création	: Mars 2017
	Rôle				: La page de modification du mot de passe
===============================================================================================================*/


// =====================================================================================================================================================
// Paramétrage de la page web
// =====================================================================================================================================================
/* Si la variable $TexteDefilant est renseignée avec :
	- ""									=> RIEN ne défilera dans l'entête de la page
	- "Defaut"								=> Le contenu du fichier texte par défaut relatif au module défilera dans l'entête de la page web
	- le nom d'un fichier texte EXISTANT	=> Le contenu de ce fichier texte défilera dans l'entête de la page web
	- le nom d'un fichier texte INEXISTANT	=> RIEN ne défilera dans l'entête de la page
	*/
$TexteDefilant = "Inscription";

// =====================================================================================================================================================
// Bandeau haut
// =====================================================================================================================================================
$Infos['BandeauHaut']['Titre'] = "Modification d'un mot de passe";

if ($TexteDefilant == "") {
    // On ne renseigne pas la variable : $Infos['BandeauHaut']['TexteDefilant']
}elseif ($TexteDefilant == "Defaut") {
    $Infos['BandeauHaut']['TexteDefilant'] = DIR_TEXTE_DEFILANT."Module".fNomDossierParent(__FILE__).".txt";
}elseif (is_file(DIR_TEXTE_DEFILANT.$TexteDefilant.".txt")) {
    $Infos['BandeauHaut']['TexteDefilant'] = DIR_TEXTE_DEFILANT.$TexteDefilant.".txt";
}else {
    // On ne renseigne pas la variable : $Infos['BandeauHaut']['TexteDefilant']
}


// =====================================================================================================================================================
// Bandeau bas
// =====================================================================================================================================================
// Null


// =====================================================================================================================================================
// Les encarts
// =====================================================================================================================================================
/*  Chaque encart est incorpor� sous la forme : $Infos['Encart'][x][] = <adresse fichier encart> (x peut prendre les valeurs 1 ou 2)
 *  si page = PageStandard					-> les encarts sont ignorés
 *  si page = PageAvecEncartsDroite			-> tous les encarts (1 et 2) sont positionnés à la suite dans la partie droite de la page
 *  si page = PageAvecEncartsGauche			-> tous les encarts (1 et 2) sont positionnés à la suite dans la partie gauche de la page
 *  si page = PageAvecEncartsGaucheDroite	-> les encarts (1) sont positionnés dans la partie gauche de la page
 *  										   les encarts (2) sont positionnés dans la partie droite de la page
 */

$Infos['Encart'][1][] = DIR_ENCART."NosPartenaires.txt";

// =====================================================================================================================================================
// Informations générales sur la page
// =====================================================================================================================================================
//$Infos['Page']['Doctype'] = "XHTML 1.0 Transitional";

// =====================================================================================================================================================
// Inclusion des fichiers nécessaires
// =====================================================================================================================================================
set_include_path(get_include_path().PATH_SEPARATOR.getcwd().DIRECTORY_SEPARATOR.LIB_PHPSECLIB);
require_once(fGetVue(__FILE__));
require_once(DIR_MODELE_PDO."class.Bd.inc.php");

$Vue['Formulaire'] = fGetLireFichier(DIR_FORM.'Form.ModifierMotDePasseUser.inc.php');

$Infos['BandeauHaut']['Titre'] = "Modification d'un mot de passe";
// =====================================================================================================================================================
// Préparation des données à fournir à la vue : Le tableau $Vue
// =====================================================================================================================================================
$Infos['Page']['Titre'] = "Modification du mot de passe utilisateur";

// =====================================================================================================================================================
// Informations concernant la partie centrale de la page + appel de la vue partielle afin de gérer son affichage
// =====================================================================================================================================================
$Infos['Page']['ContenuAvecEncarts'] = VueUtilisateur::GetXhtmlFormMotDePasse($Vue);
?>
