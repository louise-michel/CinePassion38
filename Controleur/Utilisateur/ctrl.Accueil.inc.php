<?php
/*=============================================================================================================
	Fichier				: ctrl.AfficherAccueil.inc.php (Back contrôleur)
	Auteur				: Christophe Goidin (christophe.goidin@ac-grenoble.fr)
	Date de création	: Juillet 2012
	Date de modification: Mai 2013 		-> Prise en compte de la programmation orientée objet
						  Juillet 2013	-> Certaines initialisations sont réalisées dans le contrôleur principal
						  Août 2013		-> Appel de la vue partielle pour gérer la galerie photos 
	Rôle				: La page d'accueil du site
===============================================================================================================*/


// =====================================================================================================================================================
// Paramétrage de la page web
// =====================================================================================================================================================
/* Si la variable $TexteDefilant est renseignée avec : 
	- ""									=> RIEN ne défilera dans l'entête de la page
	- "Defaut"								=> Le contenu du fichier texte par défaut relatif au module défilera dans l'entête de la page web
	- le nom d'un fichier texte EXISTANT	=> Le contenu de ce fichier texte défilera dans l'entête de la page web
	- le nom d'un fichier texte INEXISTANT	=> RIEN ne défilera dans l'entête de la page
	*/  
$TexteDefilant = "DerniersFilmsAjoutes";

// =====================================================================================================================================================
// Bandeau haut
// =====================================================================================================================================================
$Infos['BandeauHaut']['Titre'] = "Présentation du module film";

if ($TexteDefilant == "") {
	// On ne renseigne pas la variable : $Infos['BandeauHaut']['TexteDefilant']
}elseif ($TexteDefilant == "Defaut") {
	$Infos['BandeauHaut']['TexteDefilant'] = DIR_TEXTE_DEFILANT."Module".fNomDossierParent(__FILE__).".txt";
}elseif (is_file(DIR_TEXTE_DEFILANT.$TexteDefilant.".txt")) {
	$Infos['BandeauHaut']['TexteDefilant'] = DIR_TEXTE_DEFILANT.$TexteDefilant.".txt";
}else {
	// On ne renseigne pas la variable : $Infos['BandeauHaut']['TexteDefilant']
}


// =====================================================================================================================================================
// Bandeau bas
// =====================================================================================================================================================
// Null


// =====================================================================================================================================================
// Les encarts
// =====================================================================================================================================================
/*  Chaque encart est incorpor� sous la forme : $Infos['Encart'][x][] = <adresse fichier encart> (x peut prendre les valeurs 1 ou 2)
 *  si page = PageStandard					-> les encarts sont ignorés
 *  si page = PageAvecEncartsDroite			-> tous les encarts (1 et 2) sont positionnés à la suite dans la partie droite de la page
 *  si page = PageAvecEncartsGauche			-> tous les encarts (1 et 2) sont positionnés à la suite dans la partie gauche de la page
 *  si page = PageAvecEncartsGaucheDroite	-> les encarts (1) sont positionnés dans la partie gauche de la page
 *  										   les encarts (2) sont positionnés dans la partie droite de la page
 */ 

$Infos['Encart'][1][] = DIR_ENCART."NosPartenaires.txt";

// =====================================================================================================================================================
// Informations générales sur la page
// =====================================================================================================================================================
//$Infos['Page']['Doctype'] = "XHTML 1.0 Transitional";

// =====================================================================================================================================================
// Inclusion des fichiers nécessaires
// =====================================================================================================================================================
require_once(fGetVue(__FILE__));
require_once(DIR_MODELE_PDO."class.Bd.inc.php");
$Infos['BandeauHaut']['Titre'] = "Présentation du module Utilisateur";
// =====================================================================================================================================================
// Préparation des données à fournir à la vue : Le tableau $Vue
// =====================================================================================================================================================


$Infos['Page']['Titre'] = "Présentation du module Utilisateur";
$Vue['TexteAccueil'] = "
Ce site propose certaines fonctionnalités qui ne sont accessibles qu'aux utilisateurs ayant un compte. 3 niveaux de compte sont proposés.<br/><br/>
 
<strong>Le compte <I>visiteur</I></strong> <br/>
Le compte <I>visiteur</I> est accessible à tout le monde. La création d'un compte s'effectue très facilement à partir du formulaire d'inscription. Une fois inscrits, les personnes ayant un compte visiteur ont la possibilité de gérer leur compte (modification des informations personnelles, modification du mot de passe, ...). Par contre, elles n'ont pas la possibilité d'évaluer et de commenter les films proposés par notre cinémathèque, ces fonctionnalités étant réservées aux membres de notre association.<br/><br/>

<strong>Le compte <I>membre</I></strong> <br/>
Le compte <I>membre</I> n'est accessible qu'aux membres de l'association ayant acquittés leur cotisation annuelle. Un compte membre offre toutes les fonctionnalités d'un compte visiteur avec en plus la possibilité d'évaluer et de commenter les films proposés par notre cinémathèque. D'autres fonctionnalités seront implémentées prochainement et seront aussi réservés uniquement à nos membres.<br/><br/>

<strong>Le compte <I>administrateur</I></strong> <br/> 
Le compte <I>administrateur</I> n'est accessible qu'à quelques personnes seulement. Il permet d'administrer le site et donne accès à certaines statistiques sous forme de graphiques. L'administrateur a tous les droits, ce qui justifie qu'un compte de ce type ne soit réservé qu'à quelques personnes seulement.<br/><br/>

Bien évidemment, il est possible à tout moment de clôturer son compte très facilement en cliquant sur le lien approprié.
";

// =====================================================================================================================================================
// Informations concernant la partie centrale de la page + appel de la vue partielle afin de gérer son affichage
// =====================================================================================================================================================
$Infos['Page']['ContenuAvecEncarts'] = VueUtilisateur::GetXhtmlAccueil($Vue);


// =====================================================================================================================================================
// Création d'une page en lui passant en paramètre le tableau $Infos. Si ce n'est pas fait, une page "standard" sera créée par défaut dans le contrôleur principal
// =====================================================================================================================================================
$Page = new PageAvecEncartsDroite($Infos);

?>
