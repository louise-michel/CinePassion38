<?php
/*=============================================================================================================
	Fichier				: ctrl.AfficherAccueil.inc.php (Back contrôleur)
	Auteur				: Christophe Goidin (christophe.goidin@ac-grenoble.fr)
	Date de création	: Juillet 2012
	Date de modification: Mai 2013 		-> Prise en compte de la programmation orientée objet
						  Juillet 2013	-> Certaines initialisations sont réalisées dans le contrôleur principal
						  Août 2013		-> Appel de la vue partielle pour gérer la galerie photos 
	Rôle				: La page d'accueil du site
===============================================================================================================*/


// =====================================================================================================================================================
// Paramétrage de la page web
// =====================================================================================================================================================
/* Si la variable $TexteDefilant est renseignée avec : 
	- ""									=> RIEN ne défilera dans l'entête de la page
	- "Defaut"								=> Le contenu du fichier texte par défaut relatif au module défilera dans l'entête de la page web
	- le nom d'un fichier texte EXISTANT	=> Le contenu de ce fichier texte défilera dans l'entête de la page web
	- le nom d'un fichier texte INEXISTANT	=> RIEN ne défilera dans l'entête de la page
	*/  
$TexteDefilant = "Inscription";

// =====================================================================================================================================================
// Bandeau haut
// =====================================================================================================================================================
$Infos['BandeauHaut']['Titre'] = "Authentification d'un utilisateur";

if ($TexteDefilant == "") {
	// On ne renseigne pas la variable : $Infos['BandeauHaut']['TexteDefilant']
}elseif ($TexteDefilant == "Defaut") {
	$Infos['BandeauHaut']['TexteDefilant'] = DIR_TEXTE_DEFILANT."Module".fNomDossierParent(__FILE__).".txt";
}elseif (is_file(DIR_TEXTE_DEFILANT.$TexteDefilant.".txt")) {
	$Infos['BandeauHaut']['TexteDefilant'] = DIR_TEXTE_DEFILANT.$TexteDefilant.".txt";
}else {
	// On ne renseigne pas la variable : $Infos['BandeauHaut']['TexteDefilant']
}


// =====================================================================================================================================================
// Bandeau bas
// =====================================================================================================================================================
// Null


// =====================================================================================================================================================
// Les encarts
// =====================================================================================================================================================
/*  Chaque encart est incorpor� sous la forme : $Infos['Encart'][x][] = <adresse fichier encart> (x peut prendre les valeurs 1 ou 2)
 *  si page = PageStandard					-> les encarts sont ignorés
 *  si page = PageAvecEncartsDroite			-> tous les encarts (1 et 2) sont positionnés à la suite dans la partie droite de la page
 *  si page = PageAvecEncartsGauche			-> tous les encarts (1 et 2) sont positionnés à la suite dans la partie gauche de la page
 *  si page = PageAvecEncartsGaucheDroite	-> les encarts (1) sont positionnés dans la partie gauche de la page
 *  										   les encarts (2) sont positionnés dans la partie droite de la page
 */ 

$Infos['Encart'][1][] = DIR_ENCART."NosPartenaires.txt";

// =====================================================================================================================================================
// Informations générales sur la page
// =====================================================================================================================================================
//$Infos['Page']['Doctype'] = "XHTML 1.0 Transitional";

// =====================================================================================================================================================
// Inclusion des fichiers nécessaires
// =====================================================================================================================================================
set_include_path(get_include_path().PATH_SEPARATOR.getcwd().DIRECTORY_SEPARATOR.LIB_PHPSECLIB);
require_once(fGetVue(__FILE__));
require_once(DIR_MODELE_PDO."class.Bd.inc.php");
require_once(LIB_PHPSECLIB."/Crypt/RSA.php");

$PrivateKey = Bd::GetRsaPrivateKey(CRYPT_NUMRSAKEY);
     $Rsa = new Crypt_RSA();      
     $Rsa->setEncryptionMode(CRYPT_RSA_ENCRYPTION_PKCS1);      
     $Rsa->loadKey($PrivateKey);      
     $Login = $Rsa->decrypt(base64_decode($_POST['TextLogin'])); 
     $Passwd = $Rsa->decrypt(base64_decode($_POST['TextMdp']));
     $Login = htmlentities($Login, ENT_QUOTES);
     $Passwd = htmlentities($Passwd, ENT_QUOTES);
     
$ResultAuth = Bd::GetResultAuthentification($Login, $Passwd);

if(!$ResultAuth){
	$Vue['ResultAuthentification'] = false;
	bd::SetNbEchecConnexion($_SESSION['Login'], "incrementer");
}else {
	$_SESSION['Login'] 		= $ResultAuth['LoginUser'];
	$_SESSION['Nom'] 		= $ResultAuth['NomUser'];
	$_SESSION['Prenom'] 	= $ResultAuth['PrenomUser'];
	$_SESSION['Type'] 		= $ResultAuth['LibelleTypeUser'];
	
	$Vue['ResultAuthentification'] 	= true;
	$Vue['NomPrenom'] 				= $_SESSION['Nom']." ".$_SESSION['Prenom'];
	$Vue['Sexe'] 					= $ResultAuth['SexeUser'];
	$Vue['MotDePasseInitial'] 		= ($Passwd == "x");
	$Vue['Type'] 					= $_SESSION['Type'];
	$Vue['DateCreationCompte'] 		= $ResultAuth['DateHeureCreationUser'];
	$Vue['DateDerniereConnexion'] 	= $ResultAuth['DateHeureDerniereConnexionUser'];
	$Vue['NbEchecConnexion'] 		= $ResultAuth['NbEchecConnexionUser'];
	$Vue['NbTotalConnexion'] 		= $ResultAuth['NbTotalConnexionUser'];
	
	bd::SetNbEchecConnexion($_SESSION['Login'], "reinitialiser");
	bd::SetNbTotalConnexion($_SESSION['Login']);
	bd::SetDateHeureDerniereConnexion($_SESSION['Login']);
	
	VueUtilisateur::GetXhtmlAuthentification($_SESSION['Login']);
}

$Infos['BandeauHaut']['Titre'] = "Authentification d'un utilisateur";
// =====================================================================================================================================================
// Préparation des données à fournir à la vue : Le tableau $Vue
// =====================================================================================================================================================
$Infos['Page']['Titre'] = "Présentation du module Utilisateur";

// =====================================================================================================================================================
// Informations concernant la partie centrale de la page + appel de la vue partielle afin de gérer son affichage
// =====================================================================================================================================================
$Infos['Page']['ContenuAvecEncarts'] = VueUtilisateur::GetXhtmlAuthentification($Vue);

$_SESSION['Infos'] = $Infos;
// =====================================================================================================================================================
// Création d'une page en lui passant en paramètre le tableau $Infos. Si ce n'est pas fait, une page "standard" sera créée par défaut dans le contrôleur principal
// =====================================================================================================================================================

header("Location: ./Redirection.php");
?>
