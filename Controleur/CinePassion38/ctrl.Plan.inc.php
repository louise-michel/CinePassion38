<?php
/*=============================================================================================================
	Fichier				: ctrl.Plan.inc.php (Back contrôleur)
	Auteur				: Christophe Goidin (christophe.goidin@ac-grenoble.fr)
	Date de création	: Août 2013
	Date de modification:  
	Rôle				: La page permettant de situer sur un plan l'association CinePassion38
===============================================================================================================*/


// =====================================================================================================================================================
// Paramétrage de la page web
// =====================================================================================================================================================
/* Si la variable $TexteDefilant est renseignée avec : 
	- ""									=> RIEN ne défilera dans l'entête de la page
	- "Defaut"								=> Le contenu du fichier texte par défaut relatif au module défilera dans l'entête de la page web
	- le nom d'un fichier texte EXISTANT	=> Le contenu de ce fichier texte défilera dans l'entête de la page web
	- le nom d'un fichier texte INEXISTANT	=> RIEN ne défilera dans l'entête de la page
	*/  
$TexteDefilant = "Defaut";


// =====================================================================================================================================================
// Bandeau haut
// =====================================================================================================================================================
$Infos['BandeauHaut']['Titre'] = "Plan";

if ($TexteDefilant == "") {
	// On ne renseigne pas la variable : $Infos['BandeauHaut']['TexteDefilant']
}elseif ($TexteDefilant == "Defaut") {
	$Infos['BandeauHaut']['TexteDefilant'] = DIR_TEXTE_DEFILANT."Module".fNomDossierParent(__FILE__).".txt";
}elseif (is_file(DIR_TEXTE_DEFILANT.$TexteDefilant.".txt")) {
	$Infos['BandeauHaut']['TexteDefilant'] = DIR_TEXTE_DEFILANT.$TexteDefilant.".txt";
}else {
	// On ne renseigne pas la variable : $Infos['BandeauHaut']['TexteDefilant']
}


// =====================================================================================================================================================
// Bandeau bas
// =====================================================================================================================================================
// Null


// =====================================================================================================================================================
// Les encarts
// =====================================================================================================================================================
$Infos['Encart'][1][] = DIR_ENCART."AvantagesInscription.txt";
$Infos['Encart'][1][] = DIR_ENCART."DernieresActualites.txt";
$Infos['Encart'][2][] = DIR_ENCART."Meteo.txt";
$Infos['Encart'][2][] = DIR_ENCART."NosPartenaires.txt";


// =====================================================================================================================================================
// Informations générales sur la page
// =====================================================================================================================================================
//$Infos['Page']['Doctype'] = "XHTML 1.0 Transitional";
$Infos['Page']['Titre'] = "La situation géographique de notre association.";


// =====================================================================================================================================================
// Inclusion de la vue partielle pour l'affichage du contenu principal de la page
// =====================================================================================================================================================
require_once(fGetVue(__FILE__));


// =====================================================================================================================================================
// Pr�paration des données à fournir à la vue : Le tableau $Vue
// =====================================================================================================================================================
$Vue['Adresse']['Titre'] = "Adresse";
$Vue['Adresse']['Infos'] = "Notre association est située à l'adresse suivante : 30 rue Louise Michel 38100 Grenoble. Le plan Google présenté ci-dessous permet de nous situer facilement dans la ville.";

$Vue['Plan']['Titre'] = "Plan";
$Vue['Plan']['Infos'] = '<iframe width="940" height="450" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.fr/maps?q=45.168726,5.708328&amp;num=1&amp;hl=fr&amp;ie=UTF8&amp;t=m&amp;ll=45.165942,5.717783&amp;spn=0.027232,0.080681&amp;z=14&amp;output=embed"></iframe>';
$Vue['Plan']['Image'] = '<a href="https://maps.google.fr/maps?q=45.168726,5.708328&amp;num=1&amp;hl=fr&amp;ie=UTF8&amp;t=m&amp;ll=45.165942,5.717783&amp;spn=0.027232,0.080681&amp;z=14&amp;source=embed" style="color:#0000FF;text-align:left"><img alt="Agrandir" title="Se connecter � Google Maps" src="'.DIR_IMAGE_DIVERS.'Agrandir.png" /></a>';


// =====================================================================================================================================================
// Informations concernant la partie centrale de la page + appel de la vue partielle afin de gérer son affichage
// =====================================================================================================================================================
$Infos['Page']['ContenuAvecEncarts'] = VueCinePassion38::GetXhtmlContenu($Vue);


// =====================================================================================================================================================
// Création d'une page en lui passant en paramètre le tableau $Infos. Si ce n'est pas fait, une page "standard" sera créée par défaut dans le contrôleur principal
// =====================================================================================================================================================
$Page = new PageAvecEncartsGauche($Infos);

?>
