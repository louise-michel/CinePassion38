<?php
/*=============================================================================================================
	Fichier				: ctrl.Presentation.inc.php (Back contrôleur)
	Auteur				: Christophe Goidin (christophe.goidin@ac-grenoble.fr)
	Date de création	: Août 2013
	Date de modification:  
	Rôle				: La page permettant de présenter l'association CinePassion38
===============================================================================================================*/


// =====================================================================================================================================================
// Paramétrage de la page web
// =====================================================================================================================================================
/* Si la variable $TexteDefilant est renseignée avec : 
	- ""									=> RIEN ne défilera dans l'entête de la page
	- "Defaut"								=> Le contenu du fichier texte par défaut relatif au module défilera dans l'entête de la page web
	- le nom d'un fichier texte EXISTANT	=> Le contenu de ce fichier texte défilera dans l'entête de la page web
	- le nom d'un fichier texte INEXISTANT	=> RIEN ne défilera dans l'entête de la page
	*/  
$TexteDefilant = "Defaut";


// =====================================================================================================================================================
// Bandeau haut
// =====================================================================================================================================================
$Infos['BandeauHaut']['Titre'] = "Présentation de l'association";

if ($TexteDefilant == "") {
	// On ne renseigne pas la variable : $Infos['BandeauHaut']['TexteDefilant']
}elseif ($TexteDefilant == "Defaut") {
	$Infos['BandeauHaut']['TexteDefilant'] = DIR_TEXTE_DEFILANT."Module".fNomDossierParent(__FILE__).".txt";
}elseif (is_file(DIR_TEXTE_DEFILANT.$TexteDefilant.".txt")) {
	$Infos['BandeauHaut']['TexteDefilant'] = DIR_TEXTE_DEFILANT.$TexteDefilant.".txt";
}else {
	// On ne renseigne pas la variable : $Infos['BandeauHaut']['TexteDefilant']
}


// =====================================================================================================================================================
// Bandeau bas
// =====================================================================================================================================================
// Null


// =====================================================================================================================================================
// Les encarts
// =====================================================================================================================================================
$Infos['Encart'][1][] = DIR_ENCART."AvantagesInscription.txt";
$Infos['Encart'][1][] = DIR_ENCART."DernieresActualites.txt";
$Infos['Encart'][2][] = DIR_ENCART."NosPartenaires.txt";


// =====================================================================================================================================================
// Informations générales sur la page
// =====================================================================================================================================================
$Infos['Page']['Titre'] = "La présentation de notre association.";


// =====================================================================================================================================================
// Inclusion de la vue partielle pour l'affichage du contenu principal de la page
// =====================================================================================================================================================
require_once(fGetVue(__FILE__));


// =====================================================================================================================================================
// Préparation des données à fournir à la vue : Le tableau $Vue
// =====================================================================================================================================================
$Vue['Presentation']['Titre'] = "Présentation générale";
$Vue['Presentation']['Infos'] = "L'association CinePassion38 est une association loi 1901 créée à Grenoble en septembre 2008 par monsieur Joseph Martin, un passioné de cinéma depuis son enfance. Son objectif initial consistait à proposer à ses membres des films à la location selon le même principe qu'une médiathèque municipale. Aujourd'hui, l'association possède plus d'une centaine de films à la location (au format DVD et Blu-ray) dans des genres très variés afin de répondre aux besoins du plus grand nombre (fantastique, horreur, comédie romantique, drame, animation, ...).
				  					  Forte aujourd'hui de plus de 250 membres actifs, ce site internet participe à la stratégie de valorisation de l'association auprès du grand public et il se révèle aussi être un formidable vecteur de communication externe.";

$Vue['Membre']['Titre'] = "Devenir membre";
$Vue['Membre']['Infos'] = "En devenant membre de notre association, vous aurez la possibilité d'accéder librement à plus d'une centaine de films récents au format DVD et Blu-ray. Notre cinémathèque est bien évidemment en constante évolution dans la mesure où de nouveaux films sont acquis chaque mois en concertation avec nos membres afin de coller au mieux à leurs attentes en terme de contenu. Vous aurez aussi un accès privilégié à notre site internet vous permettant d'évaluer et de commenter facilement les films présents dans notre cinémathèque. En effet, en plus de faire connaître notre association au plus grand nombre, un des objectifs principaux de notre site internet est de favoriser les échanges entre nos différents membres. N'hésitez donc plus à nous rejoindre afin de vivre pleinement votre passion du cinéma avec nous.
						   Vous pouvez nous situer facilement grâce au <a class='Souligne' href='./Index.php?Page=CinePassion38&amp;Action=Plan'>plan Google</a>.";

$Vue['Organigramme']['Titre'] = "Organigramme";
$Vue['Organigramme']['Infos'] = "L'organigramme de notre association est présenté ci-dessous. Il s'agit d'une version simplifiée dans la mesure où seuls les membres qui occupent une responsabilité jugée \"importante\" y sont mentionnés.";
$Vue['Organigramme']['Image'] = "<img alt='Organigramme' src='".DIR_IMAGE_DIVERS."OrganigrammeCinePassion38.png' />";


// =====================================================================================================================================================
// Informations concernant la partie centrale de la page + appel de la vue partielle afin de gérer son affichage
// =====================================================================================================================================================
$Infos['Page']['ContenuAvecEncarts'] = VueCinePassion38::GetXhtmlContenu($Vue);


// =====================================================================================================================================================
// Création d'une page en lui passant en paramètre le tableau $Infos. Si ce n'est pas fait, une page "standard" sera créée par défaut dans le contrôleur principal
// =====================================================================================================================================================
$Page = new PageAvecEncartsGauche($Infos);

?>
