<?php
/*=============================================================================================================
	Fichier				: ctrl.Partenaires.inc.php (Back contrôleur)
	Auteur				: Christophe Goidin (christophe.goidin@ac-grenoble.fr)
	Date de création	: Août 2013
	Date de modification:  
	Rôle				: La page permettant de présenter les partenaires de l'association CinePassion38
===============================================================================================================*/


// =====================================================================================================================================================
// Paramétrage de la page web
// =====================================================================================================================================================
/* Si la variable $TexteDefilant est renseignée avec : 
	- ""									=> RIEN ne défilera dans l'entête de la page
	- "Defaut"								=> Le contenu du fichier texte par défaut relatif au module défilera dans l'entête de la page web
	- le nom d'un fichier texte EXISTANT	=> Le contenu de ce fichier texte défilera dans l'entête de la page web
	- le nom d'un fichier texte INEXISTANT	=> RIEN ne défilera dans l'entête de la page
	*/  
$TexteDefilant = "Defaut";


// =====================================================================================================================================================
// Bandeau haut
// =====================================================================================================================================================
$Infos['BandeauHaut']['Titre'] = "Nos différents partenaires";

if ($TexteDefilant == "") {
	// On ne renseigne pas la variable : $Infos['BandeauHaut']['TexteDefilant']
}elseif ($TexteDefilant == "Defaut") {
	$Infos['BandeauHaut']['TexteDefilant'] = DIR_TEXTE_DEFILANT."Module".fNomDossierParent(__FILE__).".txt";
}elseif (is_file(DIR_TEXTE_DEFILANT.$TexteDefilant.".txt")) {
	$Infos['BandeauHaut']['TexteDefilant'] = DIR_TEXTE_DEFILANT.$TexteDefilant.".txt";
}else {
	// On ne renseigne pas la variable : $Infos['BandeauHaut']['TexteDefilant']
}


// =====================================================================================================================================================
// Bandeau bas
// =====================================================================================================================================================
// Null


// =====================================================================================================================================================
// Les encarts
// =====================================================================================================================================================
$Infos['Encart'][1][] = DIR_ENCART."AvantagesInscription.txt";
$Infos['Encart'][1][] = DIR_ENCART."DernieresActualites.txt";
$Infos['Encart'][2][] = DIR_ENCART."NosPartenaires.txt";
$Infos['Encart'][2][] = DIR_ENCART."NosPartenaires.txt";


// =====================================================================================================================================================
// Informations générales sur la page
// =====================================================================================================================================================
$Infos['Page']['Titre'] = "Les différents partenaire de notre association.";


// =====================================================================================================================================================
// Inclusion de la vue partielle pour l'affichage du contenu principal de la page
// =====================================================================================================================================================
require_once(fGetVue(__FILE__));


/// =====================================================================================================================================================
// Préparation des données à fournir à la vue : Le tableau $Vue
// =====================================================================================================================================================
$Vue['Partenaire1']['Titre'] = "allocine.fr";
$Vue['Partenaire1']['Infos'] = "Offrir aux cinéphiles une source pratique et rapide d'informations sur le cinéma, tout en facilitant la réservation et l'accès aux salles, c'est la mission d'AlloCiné. AlloCiné peut être joint 24h/24 en quelques secondes par téléphone au 0.892.892.892*, sur Internet (www.allocine.com) ou via un téléphone mobile en composant le 2463 (CINE) quelque soit votre opérateur, ou sur le WAP avec Orange World, Vodafone Live ! (SFR), et I-mode (Bouygues Télécom). N'importe où, n'importe quand, et aussi dans les salles, avec AlloCiné Mag.";
$Vue['Partenaire1']['Logo']  = "<a href=\"http://www.allocine.fr\"><img alt='allocine' class=\"LogoPartenaire\" src=\"".DIR_IMAGE_PARTENAIRES."Allocine.fr-logo.png\" /></a>";

$Vue['Partenaire2']['Titre'] = "commeaucinema.com";
$Vue['Partenaire2']['Infos'] = "commeaucinema.com permet de se tenir au courant de toutes les sorties et futures sorties en salle mais offre également la consultation de dossiers et d'interviews, l'actu des stars et des séries, les bandes-annonces, les sorties DVD et Blu Ray, etc. L'entièreté du site est consultable depuis les terminaux mobiles et toutes les séances sont consultables.";
$Vue['Partenaire2']['Logo']  = "<a href=\"http://www.commeaucinema.com\"><img alt='commeaucinema' class=\"LogoPartenaire\" src=\"".DIR_IMAGE_PARTENAIRES."CommeAuCinema.com-logo.jpg\" /></a>";

$Vue['Partenaire3']['Titre'] = "premiere.fr";
$Vue['Partenaire3']['Infos'] = "première est un magazine de cinéma français, mensuel, créé en 1976 par Jean-Pierre Frimbois et Marc Esposito, aujourd'hui publié par le Groupe Lagardère. Premiere.fr est aujourd'hui le premier portail français 100% Entertainment ! Cinéma, TV, séries, people, DVD, musique, spectacle... Retrouvez l'actu du cinéma et des séries, les exclus de la rédaction et les dernières bandes annonces.";
$Vue['Partenaire3']['Logo']  = "<a href=\"http://www.premiere.fr\"><img alt='premiere' class=\"LogoPartenaire\" src=\"".DIR_IMAGE_PARTENAIRES."Premiere.fr-logo.jpg\" /></a>";


// =====================================================================================================================================================
// Informations concernant la partie centrale de la page + appel de la vue partielle afin de gérer son affichage
// =====================================================================================================================================================
$Infos['Page']['ContenuAvecEncarts'] = VueCinePassion38::GetXhtmlContenu($Vue);


// =====================================================================================================================================================
// Création d'une page en lui passant en paramètre le tableau $Infos. Si ce n'est pas fait, une page "standard" sera créée par défaut dans le contrôleur principal
// =====================================================================================================================================================
$Page = new PageAvecEncartsGauche($Infos);

?>
