<?php
/*=============================================================================================================
	Fichier				: ctrl.AfficherAccueil.inc.php (Back contrôleur)
	Auteur				: Christophe Goidin (christophe.goidin@ac-grenoble.fr)
	Date de création	: Juillet 2012
	Date de modification: Mai 2013 		-> Prise en compte de la programmation orientée objet
						  Juillet 2013	-> Certaines initialisations sont réalisées dans le contrôleur principal
						  Août 2013		-> Appel de la vue partielle pour gérer la galerie photos 
	Rôle				: La page d'accueil du site
===============================================================================================================*/


// =====================================================================================================================================================
// Paramétrage de la page web
// =====================================================================================================================================================
/* Si la variable $TexteDefilant est renseignée avec : 
	- ""									=> RIEN ne défilera dans l'entête de la page
	- "Defaut"								=> Le contenu du fichier texte par défaut relatif au module défilera dans l'entête de la page web
	- le nom d'un fichier texte EXISTANT	=> Le contenu de ce fichier texte défilera dans l'entête de la page web
	- le nom d'un fichier texte INEXISTANT	=> RIEN ne défilera dans l'entête de la page
	*/
$TexteDefilant = "DerniereMinute";

// =====================================================================================================================================================
// Bandeau haut
// =====================================================================================================================================================
$Infos['BandeauHaut']['Titre'] = "Accueil";

if ($TexteDefilant == "") {
    // On ne renseigne pas la variable : $Infos['BandeauHaut']['TexteDefilant']
} elseif ($TexteDefilant == "Defaut") {
    $Infos['BandeauHaut']['TexteDefilant'] = DIR_TEXTE_DEFILANT . "Module" . fNomDossierParent(__FILE__) . ".txt";
} elseif (is_file(DIR_TEXTE_DEFILANT . $TexteDefilant . ".txt")) {
    $Infos['BandeauHaut']['TexteDefilant'] = DIR_TEXTE_DEFILANT . $TexteDefilant . ".txt";
} else {
    // On ne renseigne pas la variable : $Infos['BandeauHaut']['TexteDefilant']
}


// =====================================================================================================================================================
// Bandeau bas
// =====================================================================================================================================================
// Null


// =====================================================================================================================================================
// Les encarts
// =====================================================================================================================================================
/*  Chaque encart est incorpor� sous la forme : $Infos['Encart'][x][] = <adresse fichier encart> (x peut prendre les valeurs 1 ou 2)
 *  si page = PageStandard					-> les encarts sont ignorés
 *  si page = PageAvecEncartsDroite			-> tous les encarts (1 et 2) sont positionnés à la suite dans la partie droite de la page
 *  si page = PageAvecEncartsGauche			-> tous les encarts (1 et 2) sont positionnés à la suite dans la partie gauche de la page
 *  si page = PageAvecEncartsGaucheDroite	-> les encarts (1) sont positionnés dans la partie gauche de la page
 *  										   les encarts (2) sont positionnés dans la partie droite de la page
 */
$Infos['Encart'][1][] = DIR_ENCART . "News.txt";
$Infos['Encart'][1][] = DIR_ENCART . "AvantagesInscription.txt";
$Infos['Encart'][1][] = DIR_ENCART . "NouvellePage.txt";

$Infos['Encart'][2][] = DIR_ENCART . "Meteo.txt";
$Infos['Encart'][2][] = DIR_ENCART . "NosPartenaires.txt";
$Infos['Encart'][2][] = DIR_ENCART . "DernieresActualites.txt";


// =====================================================================================================================================================
// Informations générales sur la page
// =====================================================================================================================================================
//$Infos['Page']['Doctype'] = "XHTML 1.0 Transitional";
$Infos['Page']['Titre'] = "CinePassion 38, l'association grenobloise pour la promotion du cinéma.";


// =====================================================================================================================================================
// Inclusion des fichiers nécessaires
// =====================================================================================================================================================
require_once(fGetVue(__FILE__));
require_once(DIR_CLASS . "class.FluxRss.inc.php");


// =====================================================================================================================================================
// Préparation des données à fournir à la vue : Le tableau $Vue
// =====================================================================================================================================================
$Vue['Titre'] = "Présentation de l'association CinePassion38";
$Vue['Contenu'] = "L'association CinePassion38 est une association loi 1901 créée à Grenoble en septembre 2008 par monsieur Joseph Martin, un passioné de cinéma depuis son enfance. Son objectif initial consistait à proposer à ses membres des films à la location selon le même principe qu'une médiathèque municipale. Aujourd'hui, l'association possède plus d'une centaine de films à la location (au format DVD et Blu-ray) dans des genres très variés afin de répondre aux besoins du plus grand nombre (fantastique, horreur, comédie romantique, drame, animation, ...).
				   Forte aujourd'hui de plus de 250 membres actifs, ce site internet participe à la stratégie de valorisation de l'association auprès du grand public et il sera aussi un formidable vecteur de communication externe. N'hésitez pas à vous inscrire afin de pouvoir profiter pleinement de l'ensemble des fonctionnalités offertes par le site, notamment la possibilité d'évaluer et de commenter les différents films proposés à la location.";

// Galerie photos SlidesJS
if (($PtrDossier = opendir(DIR_IMAGES_SLIDESJS)) !== false) {
    $TabContenu = fGetContenuFichier("./Texte/SlidesJS/Contenu.txt");
    $i = 0;
    while (($Fichier = readdir($PtrDossier)) !== false) {
        if (($Fichier != ".") AND ($Fichier != "..")) {
            $VueGalerieImage[$i] = DIR_IMAGES_SLIDESJS . $Fichier;
            $VueGalerieTexte[$i] = $TabContenu[$i];
            $i += 1;
        }
    }
    closedir($PtrDossier);
}


// =====================================================================================================================================================
// Flux RSS relatif aux actualités cinématographiques
// =====================================================================================================================================================
if (RSS_GESTION) {
    $FluxRssCinema = new FluxRss(RSS_ACTUALITE_CINEMA, "Les dernières actualités cinématographique", RSS_NB_ITEMS_PAR_FLUX);
}

// =====================================================================================================================================================
// Informations concernant la partie centrale de la page + appel de la vue partielle afin de gérer son affichage
// =====================================================================================================================================================
$Infos['Page']['ContenuSansEncarts'] = VueHome::GetXhtmlCinePassion38($Vue['Titre'], $Vue['Contenu']);
if (isset($VueGalerieImage)) {
    $Infos['Page']['ContenuSansEncarts'] .= VueHome::GetXhtmlGaleriePhotos($VueGalerieImage, $VueGalerieTexte);
}
if (RSS_GESTION) {
    $Infos['Page']['ContenuAvecEncarts'] = utf8_encode($FluxRssCinema->GetXhtmlFluxRss());
} else {
    $Infos['Page']['ContenuAvecEncarts'] = "La gestion des flux RSS est actuellement désactivée.";
}

// =====================================================================================================================================================
// Création d'une page en lui passant en paramètre le tableau $Infos. Si ce n'est pas fait, une page "standard" sera créée par défaut dans le contrôleur principal
// =====================================================================================================================================================
$Page = new PageAvecEncartsGaucheDroite($Infos);

?>
