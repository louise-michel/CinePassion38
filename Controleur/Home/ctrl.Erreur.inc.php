<?php
/*=============================================================================================================
	Fichier				: ctrl.Erreur.inc.php (Back contrôleur)
	Auteur				: Christophe Goidin (christophe.goidin@ac-grenoble.fr)
	Date de création	: Août 2013
	Date de modification: 
	Rôle				: La page d'erreur du site
===============================================================================================================*/


// =====================================================================================================================================================
// Paramétrage de la page web
// =====================================================================================================================================================
/* Si la variable $TexteDefilant est renseignée avec : 
	- ""									=> RIEN ne défilera dans l'entête de la page
	- "Defaut"								=> Le contenu du fichier texte par défaut relatif au module défilera dans l'entête de la page web
	- le nom d'un fichier texte EXISTANT	=> Le contenu de ce fichier texte défilera dans l'entête de la page web
	- le nom d'un fichier texte INEXISTANT	=> RIEN ne défilera dans l'entête de la page
	*/  
$TexteDefilant = "Defaut";


// =====================================================================================================================================================
// Bandeau haut
// =====================================================================================================================================================
$Infos['BandeauHaut']['Titre'] = "Page en construction";

if ($TexteDefilant == "") {
	// On ne renseigne pas la variable : $Infos['BandeauHaut']['TexteDefilant']
}elseif ($TexteDefilant == "Defaut") {
	$Infos['BandeauHaut']['TexteDefilant'] = DIR_TEXTE_DEFILANT."Module".fNomDossierParent(__FILE__).".txt";
}elseif (is_file(DIR_TEXTE_DEFILANT.$TexteDefilant.".txt")) {
	$Infos['BandeauHaut']['TexteDefilant'] = DIR_TEXTE_DEFILANT.$TexteDefilant.".txt";
}else {
	// On ne renseigne pas la variable : $Infos['BandeauHaut']['TexteDefilant']
}


// =====================================================================================================================================================
// Bandeau bas
// =====================================================================================================================================================
// Null


// =====================================================================================================================================================
// Les encarts
// =====================================================================================================================================================
$Infos['Encart'][1][] = DIR_ENCART."NosPartenaires.txt";
$Infos['Encart'][2][] = DIR_ENCART."NosPartenaires.txt";


// =====================================================================================================================================================
// Informations générales sur la page
// =====================================================================================================================================================
$Infos['Page']['Titre'] = "CinePassion 38. A la découverte du cinéma...";


// =====================================================================================================================================================
// Inclusion de la vue partielle pour l'affichage du contenu principal de la page
// =====================================================================================================================================================
require_once(fGetVue(__FILE__));


// =====================================================================================================================================================
// Préparation des données à fournir à la vue : Le tableau $Vue
// =====================================================================================================================================================
$Vue['Contenu'] = "Cette page a été supprimée ou elle est actuellement en construction. Elle sera publiée dès que possible. Merci d'avance pour votre indulgence et votre compréhension pour ce désagrément temporaire.";


// =====================================================================================================================================================
// Informations concernant la partie centrale de la page + appel de la vue partielle afin de gérer son affichage
// =====================================================================================================================================================
$Infos['Page']['ContenuSansEncarts'] = "Bloc ContenuSansEncarts Bloc ContenuSansEncarts Bloc ContenuSansEncarts Bloc ContenuSansEncarts Bloc ContenuSansEncarts Bloc ContenuSansEncarts Bloc ContenuSansEncarts Bloc ContenuSansEncarts Bloc ContenuSansEncarts Bloc ContenuSansEncarts Bloc ContenuSansEncarts Bloc ContenuSansEncarts Bloc ContenuSansEncarts Bloc ContenuSansEncarts Bloc ContenuSansEncarts ";
$Infos['Page']['ContenuAvecEncarts'] = VueHome::GetXhtmlErreur($Vue['Contenu']);


// =====================================================================================================================================================
// Création d'une page en lui passant en paramètre le tableau $Infos. Si ce n'est pas fait, une page "standard" sera créée par défaut dans le contrôleur principal
// =====================================================================================================================================================
$Page = new PageAvecEncartsGaucheDroite($Infos);

?>
