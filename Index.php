<?php
/*=============================================================================================================
	Fichier				: Index.php (Front Contrôleur) 
	Auteur				: Christophe Goidin (christophe.goidin@ac-grenoble.fr)
	Date de création	: Juillet 2012
	Modification		: Août 2013 ->	Configuration locale en français 
	Rôle				: Le FRONT Contrôleur joue le rôle d'aiguillage principal vers les BACK Contrôleurs
===============================================================================================================*/
session_start();

if(isset($_SESSION['Infos'])) {
	unset($_SESSION['Infos']);
}

header('content-type: text/html; charset=utf-8');
// =====================================================================================================================================================
// Modification des chemins d'accès aux dossiers, utilisé lors de l'inclusion des fichiers (fonction php : require ou include) 
// =====================================================================================================================================================
/* Chemin de recherche des fichiers 
 *	PATH_SEPARATOR :		;
 *	DIRECTORY_SEPARATOR :	/ sous Linux, \ sous Windows mais le / est aussi accepté
 */
set_include_path(get_include_path(). PATH_SEPARATOR . getcwd() . DIRECTORY_SEPARATOR . "Include");
set_include_path(get_include_path(). PATH_SEPARATOR . getcwd() . DIRECTORY_SEPARATOR . "Class");
set_include_path(get_include_path(). PATH_SEPARATOR . getcwd() . DIRECTORY_SEPARATOR . "Controleur");
set_include_path(get_include_path(). PATH_SEPARATOR . getcwd() . DIRECTORY_SEPARATOR . "Vue");


// =====================================================================================================================================================
// Inclusion des fichiers nécessaires à toutes les pages du site
// =====================================================================================================================================================
require_once("Config.inc.php");
require_once("Fonction.lib.php");
require_once("class.vue.Pages.inc.php");
require_once("class.Collection.inc.php");
require_once("class.Encart.inc.php");

// =====================================================================================================================================================
// Inclusion des fichiers nécessaires uniquement pour certaines pages du site
// =====================================================================================================================================================
// La classe FluxRss est nécessaire uniquement dans la page d'accueil => 2 cas possibles : premier chargement du site (la chaîne de requête vaut "") OU demande explicite par l'utilisateur de la page d'accueil (la chaîne de requête vaut alors "Page=Accueil")
if ((strlen($_SERVER['QUERY_STRING']) == 0) OR ($_SERVER['QUERY_STRING']) == "Page=Accueil") {
	require_once("class.FluxRss.inc.php");
}
// Utilisation de la classe FluxRss aussi en mode DEBUG
if (substr($_SERVER['QUERY_STRING'], 0, 20) == "XDEBUG_SESSION_START") {
	require_once("class.FluxRss.inc.php");
}


// =====================================================================================================================================================
// Modification de la configuration locale
// =====================================================================================================================================================
setlocale(LC_TIME, "fra");	// Pour que les dates/heures s'affichent en français


// =====================================================================================================================================================
// Début de la tamporisation de sortie
// =====================================================================================================================================================
ob_start();

$Infos['BandeauHaut']['Authentification'] = fGetLireFichier(DIR_FORM."Form.AuthentificationUser.inc.php");
// =====================================================================================================================================================
// Informations communes à toutes les pages du site
// =====================================================================================================================================================


$Infos['BandeauHaut']['CheminDeFer'] = "";
if (!isset($_REQUEST['Page']) OR ($_REQUEST['Page'] == "Accueil")) {
	$Infos['BandeauHaut']['Version'] = CONF_VERSION;
	$Infos['BandeauHaut']['DateDuJour'] = strftime("Nous sommes le %A %d %B %Y");
}else {
	$Infos['BandeauHaut']['Version'] = "";
	$Infos['BandeauHaut']['DateDuJour'] = "";
}
$Infos['BandeauBas']['Copyright'] = CONF_COPYRIGHT;
$Infos['Page']['Doctype'] = "XHTML 1.0 Strict";	// Le doctype est redéfini si nécessaire dans les back contrôleurs adéquats
$Infos['Page']['TitreNavigateur'] = CONF_TITRE_NAVIGATEUR;

// Savoir si l'utilisateur est connecté ou non et charger la clé publique en fonction de la connexion

if ((!isset($_SESSION['Login']))
	|| (isset($_SESSION['Login']) && $_REQUEST['Page'] == "Utilisateur" && $_REQUEST['Action'] == "ModifierInfos")
	|| (isset($_SESSION['Login']) && $_REQUEST['Page'] == "Utilisateur" && $_REQUEST['Action'] == "ModifierMotDePasse")
	|| (isset($_SESSION['Login']) && $_REQUEST['Page'] == "Utilisateur" && $_REQUEST['Action'] == "Deconnexion")){
	require_once(DIR_MODELE_PDO."class.Bd.inc.php");
	$Infos['Page']['RsaPublicKey'] = preg_replace("/(\r\n|\n|\r)/","",Bd::GetRsaPublicKey(CRYPT_NUMRSAKEY));
}

// =====================================================================================================================================================
// Appel du BACK Contrôleur - Le tableau $Infos est mis à jour et la page est créée normalement
// =====================================================================================================================================================
if (!isset($_REQUEST['Page'])) {
	require_once(DIR_CONTROLEUR."Home/ctrl.AfficherAccueil.inc.php");
//}elseif ($_REQUEST['Page'] == "Accueil") {
//	require_once("ctrl.Accueil.inc.php");
}else {
	if (!isset($_REQUEST['Action'])) {
		if ($_REQUEST['Page'] == "Home") {
			require_once(DIR_CONTROLEUR."Home/ctrl.AfficherAccueil.inc.php");
		}else {
			require_once(DIR_CONTROLEUR."Home/ctrl.Erreur.inc.php");
		}
	}else {
		if (is_file(DIR_CONTROLEUR.$_REQUEST['Page']."/ctrl.".$_REQUEST['Action'].".inc.php") AND is_file("./Vue/class.vue.".$_REQUEST['Page'].".inc.php")) {
			require_once(DIR_CONTROLEUR.$_REQUEST['Page']."/ctrl.".$_REQUEST['Action'].".inc.php");
		}else {
			require_once(DIR_CONTROLEUR."Home/ctrl.Erreur.inc.php");
		}
	}
}


// =====================================================================================================================================================
// Création d'une page standard si elle n'a pas été créée dans le BACK contrôleur
// =====================================================================================================================================================
if (!isset($Page)) {
	$Page = new PageStandard($Infos);
}


// =====================================================================================================================================================
// Affichage de la page
// =====================================================================================================================================================
echo $Page->GetXhtmlPage();


// =====================================================================================================================================================
// Fin de la tamporisation de sortie et affichage de la page
// =====================================================================================================================================================
echo ob_get_clean();

?>
