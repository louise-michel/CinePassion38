<?php
/*==================================================================================
    Fichier 			: Config.inc.php
	Auteur  			: Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	Date de création 	: Juillet 2012
	Date de modification: Août 2013 - Modification des noms de constantes
	But     			: Contient tout le paramétrage du site
====================================================================================*/

// =====================================================================================================================================================
// Configuration générale : préfixe -> CONF_...
// =====================================================================================================================================================
define("CONF_TITRE_NAVIGATEUR", "CinePassion38");
define("CONF_VERSION", "Version 1.0");
define("CONF_COPYRIGHT", "CinePassion38 - L'association grenobloise pour la promotion du cinéma<br/>@Copyright 2013 Genesys - Tous droits réservés");
define("CONF_EDUCATION", "Education nationale");
define("CONF_ACADEMIE", "Académie de Grenoble");
define("CONF_LYCEE", "Lycée Louise Michel");
define("CONF_PARTENAIRE_1_NOM", "AlloCine.fr");
define("CONF_PARTENAIRE_1_SITE", "http://www.allocine.fr");
define("CONF_PARTENAIRE_2_NOM", "CommeAuCinema.com");
define("CONF_PARTENAIRE_2_SITE", "http://www.commeaucinema.com");
define("CONF_PARTENAIRE_3_NOM", "Premiere.fr");
define("CONF_PARTENAIRE_3_SITE", "http://www.premiere.fr");
define("CONF_PARTENAIRE_4_NOM", "Lycée Louise Michel");
define("CONF_PARTENAIRE_4_SITE", "http://www.ac-grenoble.fr/lycee/louise.michel/index.php?lng=fr");
define("CONF_OS", "Windows");
define("CONF_MINIFY", false);				// utilisation des scripts "minifiés" -> .min.js, .min.css


// =====================================================================================================================================================
// Base de données : préfixe -> BD_...
// =====================================================================================================================================================
define("BD_SERVER", "localhost");
define("BD_USER", "root");
define("BD_PWD", "");
define("BD_NAME", "CinePassion38_Groupe7");


// =====================================================================================================================================================
// Adressage des dossiers : préfixe -> DIR_...
// =====================================================================================================================================================
define("DIR_CLASS", "./Class/");
define("DIR_CONTROLEUR","./Controleur/");
define("DIR_VUE", "./Vue/");
define("DIR_INCLUDE", "./Include/");
define("DIR_MODELE_PDO", "./Modele/");
define("DIR_CSS", "./Css/");
define("DIR_JS", "./Javascript/");
define("DIR_FORM", "./Formulaire/");
define("DIR_ENCART", "./Texte/Encart/");
define("DIR_IMAGE_CINE_PASSION", "./Image/CinePassion/");
define("DIR_IMAGE_DIVERS", "./Image/Divers/");
define("DIR_IMAGE_PARTENAIRES", "./Image/Partenaires/");
define("DIR_IMAGES_SLIDESJS", "./Image/SlidesJS/");
define("DIR_TEXTE_CONTENU", "./Texte/Contenu/");
define("DIR_TEXTE_DEFILANT", "./Texte/Defilant/");
define("DIR_FILM_AFFICHE", "./Image/Film/Affiche");
define("PAS_DE_PHOTO", "./Image/Personne/Pas de photo.jpg");

// =====================================================================================================================================================
// Adressage des librairies : préfixe -> LIB_...
// =====================================================================================================================================================
define("LIB_JQUERY", "./Librairie/JQuery/");
define("LIB_DIST_JQUERY", "https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js");
define("LIB_LISCROLL", "./Librairie/LiScroll/");
define("LIB_SLIDESJS", "./Librairie/SlidesJS/");
define("LIB_SIMPLESLIDESHOW", "./Librairie/SimpleSlideShow/");
define("LIB_LIGHTBOX2", "./Librairie/LightBox2/");
define("LIB_DIST_LIGHTBOX2", "https://github.com/lokesh/lightbox2/tree/master/dist/js/lightbox-plus-jquery.min.js");
define("LIB_JSENCRYPT", "./Librairie/JsEncrypt/jsencrypt-master/bin/");
define("LIB_PHPSECLIB", "./Librairie/PHPSecLib/");
define('LIB_PWDSECURE', './Librairie/PwdSecure/');

// =====================================================================================================================================================
// Gestion des flux RSS : préfixe -> RSS_...
// =====================================================================================================================================================
define("RSS_GESTION", false);				// true si les flux Rss sont gérés, false sinon
define("RSS_NB_ITEMS_PAR_FLUX", 10);		// nombre d'éléments du flux RSS à récupérer
define("RSS_FILM_DE_LA_SEMAINE", "http://rss.allocine.fr/ac/cine/cettesemaine");
define("RSS_FILM_PROCHAINEMENT", "http://rss.allocine.fr/ac/cine/prochainement");
define("RSS_ACTUALITE_CINEMA", "http://www.premiere.fr/var/premiere/storage/rss/cinema_actu.xml");
define("RSS_ACTUALITE_PEOPLE", "http://people.premiere.fr/var/premiere/storage/rss/people_actu.xml");	

// =====================================================================================================================================================
// Gestion des films présentés
// =====================================================================================================================================================
define("NB_FILM_SLIDESHOW",20);
define("EXTENSION_AFFICHE","jpg");
define("NAV_NB_LIGNESPARPAGE",10);
define("NAV_NUM_AVAP", 2);

// =====================================================================================================================================================
// Gestion des authentifications
// =====================================================================================================================================================
define("CRYPT_NUMRSAKEY",4);
define("USER_DUREE_DECONNEXION_AUTO",300);

?>
