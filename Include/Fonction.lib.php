<?php
/*======================================================================================================
    Fichier 			: Fonction.lib.php
	Auteur  			: Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	Date de création 	: Juillet 2012
	Date de modification: Août 2013
	But     			: Contient toutes les fonctions utiles du site
========================================================================================================*/


// =====================================================================================================================================================
// Les fonctions générales
// =====================================================================================================================================================
/**
 * Teste si le nombre passé en paramètre est pair ou non
 * @param integer $pNb : le nombre à tester
 * @return boolean : true si $pNb est pair, false sinon
 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 * @version 1.0
 * @copyright Christophe Goidin - Juillet 2012
 */
function fEstPair($pNb) {
    return ($pNb%2 == 0);
}

/**
 * Teste si le nombre passé en paramètre est un nombre entier ou non
 * @param integer $pNb : le nombre à tester
 * @return boolean : true si $pNb est un nombre entier, false sinon
 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 * @version 1.0
 * @copyright Christophe Goidin - Juillet 2012
 * @deprecated
 */

function fEstEntier($pNb) {
	return ($pNb == floor($pNb));
}

/**
 * Renvoie l'adresse relative de la vue correspondant au contrôleur passé en paramètre
 * @param string $pControleur : l'adresse relative du contrôleur
 * @return string : l'adresse relative de la vue correspondant au contrôleur passé en paramètre
 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 * @version 1.0
 * @copyright Christophe Goidin - Août 2013
 */
function fGetVue($pControleur) {
	if (CONF_OS == "Windows") {
		$Tab = explode("\\", $pControleur);
	}else { //Unix, Linux
		$Tab = explode("/", $pControleur);
	}
	if ($Tab[count($Tab)-2] == "Controleur") { // Page accueil du site ou page d'erreur
		return "./Vue/".str_replace("ctrl", "vue", $Tab[count($Tab)-1]);
	}else {
		return "./Vue/class.vue.".$Tab[count($Tab)-2].".inc.php";	
	}
}

/**
 * Renvoie le nom du dossier parent du fichier dont l'adresse absolue est passée en paramètre
 * @param string $pChaine : l'adresse absolue du fichier
 * @return string : le nom du dossier parent
 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 * @version 1.0
 * @copyright Christophe Goidin - Juin 2014
 */
function fNomDossierParent($pChaine) {
	if (CONF_OS == "Windows") {
		$Tab = explode("\\", $pChaine);
	}else { //Unix, Linux
		$Tab = explode("/", $pChaine);
	}
	return $Tab[count($Tab)-2];
}

/**
 * Renvoie les différents éléments de l'adresse absolue du fichier passé en paramètre dispatchés dans un tableau
 * @param string $pChaine : l'adresse absolue du fichier à dispatcher dans le tableau
 * @return array : un tableau contenant les différents éléments dispatchés du fichier passé en paramètre
 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 * @version 1.0
 * @copyright Christophe Goidin - Août 2013
 */
function fDispatcherAdresse($pChaine) {
	if (CONF_OS == "Windows") {
		$Tab = explode("\\", $pChaine);
	}else { //Unix, Linux
		$Tab = explode("/", $pChaine);
	}
	return $Tab;
}

// =====================================================================================================================================================
// Dossiers et fichiers
// =====================================================================================================================================================
/**
 * Renvoie le nom du module (dossier de niveau le plus fin)
 * @param string $pDossier : l'adresse absolue du dossier dans lequel la recherche s'effectue
 * @return string : le nom du dossier de niveau le plus fin
 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 * @version 1.0
 * @copyright Christophe Goidin - Juillet 2012
 * @example fGetNomDossier("C:\wamp\Cinema\Films") -> Films
 * @deprecated
 */
function fGetNomModule($pDossier) {
	if (CONF_OS == "Windows") {
		return substr($pDossier,(strrpos($pDossier,"\\")+1));
	}else { //Unix, Linux
		return substr($pDossier,(strrpos($pDossier,"/")+1));
	}
}


/**
 * Renvoie les lignes du fichier texte passé en paramètre sous forme d'un tableau
 * @param string $pAdrFichier : l'adresse relative du fichier texte à lire
 * @return array : un tableau contenant les lignes du fichier texte $pAdrFichier
 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 * @version 1.0
 * @copyright Christophe Goidin - Juillet 2012
 */
function fGetContenuFichier($pAdrFichier) {
	if (($RefFichier = fopen($pAdrFichier, "r")) !== False) {
		$i=0;
		while (!feof($RefFichier)) {
        	$Result[$i++] = fgets($RefFichier);
		}
		fclose($RefFichier);
		return $Result;		
	}else {
		 echo "erreur d'ouverture du fichier : ".$pNomFichier;
		 return false;
	}
}

/**
 * Renvoie le nombre d'images de type $pExtension présentes dans $pDossier
 * @param string $pAdrFichier : l'adresse relative du fichier texte à lire
 * @return array : un tableau contenant les lignes du fichier texte $pAdrFichier
 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 * @version 1.0
 * @copyright Christophe Goidin - Juillet 2012
 */

function fGetNbFichiers($pDossier, $pExtension) {
	if (($PtrDossier = opendir($pDossier)) !== false){
		$i = 0;
		while(($Fichier = readdir($PtrDossier)) !== false){
			if (($Fichier != ".") AND ($Fichier != "..") AND ((pathinfo($Fichier)['extension']) == $pExtension)){
				$i++;
			}
		}
	}
	closedir($PtrDossier);
	return $i;
}

/**
 * Renvoie les informations du fichier selectionnée
 * @param string $PtrDossier : Représente l'index du fichier pointé.
 * @param string $Fichier : Représente le fichier 
 * @param string $pNum : Représente l'emplacement du fichier dans le dossier 
 * @param string $pExclusion : Représente le nom du fichier à exclure 
 * @return $Fichier : le nom du fichier a renvoyer
 * @author Clement Vitti
 * @version 1.0
 * @copyright Clement Vitti
 */

function fGetUneImage($pDossier, $pExtension, $pNum, $pExclusion = "Aucune Affiche") {
	if (($PtrDossier = opendir($pDossier)) !== false){
		$i = 0;
		do{
			$Fichier = readdir($PtrDossier);
			if(($Fichier != ".") AND ($Fichier != "..") AND (substr($Fichier, strlen($Fichier)-3,3) == $pExtension) && ($Fichier != $pExclusion.$pExtension)){
				$i++;
			}
		}while ($i <= $pNum);
		closedir($PtrDossier);
		return $Fichier;
	}
	return false;
}

/**
 * Renvoie le nombre de pages calculé à partir du nombre total d'enregistrements ainsi que du nombre d'enregistrements par page.
 * @param int -	$pNbLignes : le nombre total d'enregistrements
 * @param int -	$pNbLignesParPage : le nombre d'enregistrements par page
 * @return Nombre de pages necessaires pour afficher tous les enregistrements
 * @author Clement Vitti
 * @version 1.0
 * @copyright Clement Vitti
 */

function fGetNbPages($pNbLignes, $pNbLignesParPage = NAV_NB_LIGNESPARPAGE) {
	return ceil($pNbLignes / $pNbLignesParPage);
}

/**
 * Lit un fichier texte et retourne son contenu
 * @param string $pAdrFichier : l'adresse du fichier texte à lire
 * @return string : le contenu du fichier (ou false si erreur)
 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 * @version 1.0
 * @copyright Christophe Goidin - Juillet 2012
 */
function fGetLireFichier($pAdrFichier) {
	if (is_file($pAdrFichier)) {
		ob_start();
		include $pAdrFichier;
		return ob_get_clean();
	}
	return false;
}


?>
