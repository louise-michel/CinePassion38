<?php
/*=============================================================================================================
	Fichier				: class.FluxRss.inc.php
	Auteur				: Christophe Goidin (christophe.goidin@ac-grenoble.fr)
	Date de création	: Juillet 2013
	Date de modification: Août 2013 -> prise en compte des sélecteurs CSS 
	Rôle				: Décrit la classe FluxRss qui permet de gérer les flux RSS
===============================================================================================================*/

/**
 * La classe FluxRss permet de gérer les flux RSS
 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 * @version 1.0
 * @copyright Christophe Goidin - Juillet 2013
 */
class FluxRss {
	
	// =====================================================================================================================================================
	// Les attributs
	// =====================================================================================================================================================
	private $TabFlux;					// Le tableau contenant les flux récupérés
	private $NbMaxElements;				// Le nombre maximum d'éléments à récupé�rer dans le flux RSS
	private $Titre;						// Le titre du flux
	private $SelecteurCssFlux;			// Le sélecteur CSS relatif au flux RSS
	private $SelecteurCssTitre;			// Le sélecteur CSS relatif au titre du flux RSS
	
	
	// =====================================================================================================================================================
	// Le constructeur
	// =====================================================================================================================================================
	/**
	 * Le constructeur permet d'hydrater tous les attributs de la classe FluxRss en appelant les setteurs appropriés
	 * @param string $pAdresseFluxRss : l'adresse du flux RSS à récupérer
	 * @param string $pTitre : le titre du flux RSS
	 * @param integer $pNbMaxElements : le nombre maximal d'éléments à récupérer (10 par défaut)
	 * @param string $SelecteurCssFlux : le sélecteur CSS relatif au flux RSS (valeur par défaut : "FluxRss")
	 * @param string $SemecteurCssTitre : le sélecteur CSS relatif au titre du flux RSS (valeur par défaut : "TitreFluxRss")
	 * @return null
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Juillet 2013
	 */	
	public function __construct($pAdresseFluxRss, $pTitre, $pNbMaxElements = 10, $pSelecteurCssFlux = "FluxRss", $pSelecteurCssTitre = "TitreFluxRss") {
		$TabFlux = array();
		$this->SetTitre($pTitre);
		$this->SetNbMaxElements($pNbMaxElements);
		$this->SetSelecteurCssFlux($pSelecteurCssFlux);
		$this->SetSelecteurCssTitre($pSelecteurCssTitre);
		$this->LoadFlux($pAdresseFluxRss);
	}
		

	// =====================================================================================================================================================
	// Les accesseurs (ou getter)
	// =====================================================================================================================================================
	/**
	 * Renvoie un tableau comprenant les flux RSS
	 * @param null 
	 * @return array : le tableau des flux RSS
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Juillet 2013
	 */
	private function GetFlux(){
		return $this->TabFlux;
	}
	
	/**
	 * Renvoie le nombre d'éléments composant le flux RSS
	 * @param null 
	 * @return integer : le nombre d'éléments composant le flux RSS 
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Juillet 2013
	 */
	private function GetNbMaxElements() {
		return $this->NbMaxElements;
	}
	
	/**
	 * Renvoie le titre du flux Rss
	 * @param null 
	 * @return string : le titre du flux Rss
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Juillet 2013
	 */
	private function GetTitre() {
		return $this->Titre;
	}

	/**
	 * Renvoie le sélecteur CSS relatif au flux RSS
	 * @param null 
	 * @return string : le sélecteur CSS relatif au flux RSS 
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Août 2013
	 */
	private function GetSelecteurCssFlux() {
		return $this->SelecteurCssFlux;
	}
	
	/**
	 * Renvoie le sélecteur CSS relatif au titre du flux RSS
	 * @param null 
	 * @return string : le sélecteur CSS relatif au titre du flux RSS 
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Août 2013
	 */
	private function GetSelecteurCssTitre() {
		return $this->SelecteurCssTitre;
	}
	
	
	// =====================================================================================================================================================
	// Les mutateurs (ou setter)
	// =====================================================================================================================================================
	/**
	 * Positionne le nombre d'éléments maximum du flux RSS
	 * @param integer $pNbMaxElements : le nombre maximum d'éléments du flux RSS
	 * @return null
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Juillet 2013
	 */
	private function SetNbMaxElements($pNbMaxElements) {
		$this->NbMaxElements = $pNbMaxElements;
	}
	
	/**
	 * Positionne le titre du flux RSS
	 * @param string $pTitre : le Titre du flux Rss
	 * @return null
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Juillet 2013
	 */
	private function SetTitre($pTitre) {
		$this->Titre = $pTitre;
	}
	
	/**
	 * Positionne le sélecteur CSS relatif au flux RSS
	 * @param string $pValue : le sélecteur CSS relatif au flux RSS
	 * @return null
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Août 2013
	 */
	private function SetSelecteurCssFlux($pValue) {
		$this->SelecteurCssFlux = $pValue;
	}
	
	/**
	 * Positionne le sélecteur CSS relatif au titre du flux RSS
	 * @param string $pValue : le sélecteur Css relatif au titre du flux RSS
	 * @return null
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Août 2013
	 */
	private function SetSelecteurCssTitre($pValue) {
		$this->SelecteurCssTitre = $pValue;
	}
	
	
	// =====================================================================================================================================================
	// Les autres méthodes
	// =====================================================================================================================================================
	/**
	 * Lit le flux RSS passé en paramètre et alimente le tableau avec les informations récupérées
	 * @param string : l'adresse du flux RSS à analyser
	 * @return null 
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Août 2013
	 */
	private function LoadFlux($pAdresseFluxRss) {
		try {
			$Rss = @simplexml_load_file($pAdresseFluxRss); //@ pour supprimer les éventuels WARNING en cas d'échec (si le fichier XML n'existe pas par exemple)
			if (!$Rss) {
				//throw new Exception("Impossible de lire le flux Rss.", 0);
			}else {
				for ($i=0; $i<$this->GetNbMaxElements() AND $i<$Rss->channel->item->count(); $i++) {
					$this->TabFlux[$i]['Image'] = $Rss->channel->item[$i]->enclosure['url'];
					$this->TabFlux[$i]['DateHeure'] = utf8_decode(date("j M Y - g:i:s", strtotime($Rss->channel->item[$i]->pubDate)));
					$this->TabFlux[$i]['Titre'] = utf8_decode($Rss->channel->item[$i]->title);
					$this->TabFlux[$i]['Description'] = utf8_decode($Rss->channel->item[$i]->description);
				}
			}
		} catch (Exception $e) {
			//echo "PB FLUX RSS" . $e->getMessage();
		}
	}	
	
	/**
	 * Renvoie le bloc XHTML relatif au flux RSS (ou le message "Flux RSS introuvable" si le flux RSS n'a pas été trouvé)
	 * @param null
	 * @return string : le bloc XHTML relatif au flux RSS 
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Juillet 2013
	 */
	public function GetXhtmlFluxRss() {
		$Chaine = "<div class='".$this->GetSelecteurCssFlux()."'><span class='".$this->GetSelecteurCssTitre()."'>".utf8_decode($this->GetTitre())."</span>";	
		if (count($this->TabFlux) == 0) {	// Le flux RSS n'a pas été trouvé
			$Chaine .= "Flux RSS introuvable<br/><br/>";
		}else {
			foreach ($this->GetFlux() as $i=>$UneActuCinema) {
				$Chaine .= "<div class='Gauche'><a href='".$UneActuCinema['Image']."' rel='lightbox[Cinema$i]'><img alt='' src='".$UneActuCinema['Image']."' /></a></div>
							<div class='Droite'>
								<span class='PresentationUnFlux'>
									<span class='DateHeure'>".$UneActuCinema['DateHeure']."</span>
									<span class='Titre'>".$UneActuCinema['Titre']."</span>
								</span>
								<span class='Description'>".strip_tags($UneActuCinema['Description'])."</span>
							</div><hr/>\n";	// la fonction strip_tags permet de supprimer les balises <p> </p>
			}
		}
		$Chaine .= "<a title='Flux RSS' onclick='window.open(this.href); return false;' href='http://fr.wikipedia.org/wiki/RSS'><img alt='Flux Rss' src='".DIR_IMAGE_DIVERS."Rss.png' /></a></div>";
		return $Chaine;
	}
	
	/**
 	 * Méthode MAGIQUE appelée automatiquement lorsque l'utilisateur essaie d'afficher un objet de la classe. La méthode GetXhtmlFluxRss() est alors appelée.
 	 * @param null
 	 * @return string : le bloc XHTML relatif au flux RSS 
 	 * @author : Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 	 * @version : 1.0
 	 * @copyright Christophe Goidin - Juillet 2013
 	 */
 	public function __toString() {
        return $this->GetXhtmlFluxRss();
    }

} // fin class

?>