<?php
/*=============================================================================================================
	Fichier				: class.Encart.inc.php
	Auteur				: Christophe Goidin (christophe.goidin@ac-grenoble.fr)
	Date de cr�ation	: Mai 2013
	Date de modification:  
	Rôle				: Décrit la classe Encart qui permet de gérer les encarts d'une page web
===============================================================================================================*/

/**
 * La classe Encart permet de gérer les encarts
 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 * @version 1.0
 * @copyright Christophe Goidin - Mai 2013
 */
class Encart {
	
	// =====================================================================================================================================================
	// Les attributs
	// =====================================================================================================================================================
	private $Titre;					// Le titre de l'encart
	private $Contenu;				// Le contenu de l'encart
	private $SelecteurCssEncart;	// Le sélecteur CSS relatif à l'encart
	private $SelecteurCssTitre;		// Le sélecteur CSS relatif au titre de l'encart
	
	
	// =====================================================================================================================================================
	// Le constructeur
	// =====================================================================================================================================================
	/**
	 * Le constructeur permet d'hydrater tous les attributs de la classe Encart en appelant les setteurs appropriés
	 * @param string $pNomFichierEncart : le nom et l'adresse du fichier texte concenant les informations de l'encart
	 * @param string $pSelecteurCssEncart : le sélecteur CSS relatif à l'encart (valeur par défaut : "Encart")
	 * @param string $pSelecteurCssTitre : le sélecteur Css relatif au titre de l'encart (valeur par défaut : "Titre")
	 * @return null
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */	
	public function __construct($pNomFichierEncart, $pSelecteurCssEncart = "Encart", $pSelecteurCssTitre = "Titre") {
		$this->SetEncart($pNomFichierEncart);
		$this->SetSelecteurCssEncart($pSelecteurCssEncart);
		$this->SetSelecteurCssTitre($pSelecteurCssTitre);
	}
	
	
	// =====================================================================================================================================================
	// Les accesseurs (ou getter)
	// =====================================================================================================================================================
	/**
	 * Renvoie le titre de l'encart
	 * @param null 
	 * @return string : le titre de l'encart 
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	private function GetTitre(){
		return $this->Titre;
	}
	
	/**
	 * Renvoie le contenu de l'encart
	 * @param null 
	 * @return string : le contenu de l'encart 
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	private function GetContenu() {
		return $this->Contenu;
	}
	
	/**
	 * Renvoie le sélecteur CSS relatif 0 l'encart
	 * @param null 
	 * @return string : le s2lecteur CSS relatif à l'encart 
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	private function GetSelecteurCssEncart() {
		return $this->SelecteurCssEncart;
	}
	
	/**
	 * Renvoie le sélecteur CSS relatif au titre de l'encart
	 * @param null 
	 * @return string : le sélecteur Css relatif au titre de l'encart 
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	private function GetSelecteurCssTitre() {
		return $this->SelecteurCssTitre;
	}
	
	
	// =====================================================================================================================================================
	// Les mutateurs (ou setter)
	// =====================================================================================================================================================
	/**
	 * Positionne le titre de l'encart
	 * @param string $pValue : le titre de l'encart
	 * @return null
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	private function SetTitre($pValue) {
		$this->Titre = $pValue;
	}
	
	/**
	 * Positionne le contenu de l'encart
	 * @param string $pValue : le contenu de l'encart
	 * @return null
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	private function SetContenu($pValue) {
		$this->Contenu = $pValue;
	}
	
	/**
	 * Positionne le sélecteur CSS relatif à l'encart
	 * @param string $pValue : le sélecteur CSS relatif à l'encart
	 * @return null
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	private function SetSelecteurCssEncart($pValue) {
		$this->SelecteurCssEncart = $pValue;
	}
	
	/**
	 * Positionne le sélecteur CSS relatif au titre de l'encart
	 * @param string $pValue : le sélecteur CSS relatif au titre de l'encart
	 * @return null
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	private function SetSelecteurCssTitre($pValue) {
		$this->SelecteurCssTitre = $pValue;
	}
	
	
	// =====================================================================================================================================================
	// Les autres méthodes
	// =====================================================================================================================================================
	/**
	 * Positionne le titre et le contenu de l'encart à partir du fichier texte passé en paramètre
	 * 
	 * La structure du fichier texte doit obligatoirement être la suivante :
	 * ======================================================
	 *    Titre : xxx
	 * ======================================================
	 * UneLigne
	 * #UneLigne	si une ligne commence par le caractère #, alors le contenu de la ligne sera centré et sera affiché dans une autre couleur
	 * *UneLigne	si une ligne commence par le caractère *, alors une image sera insérée avant le contenu de la ligne
	 * 
	 * @param string $pNomFichierEncart : l'adresse du fichier texte à analyser
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
 	private function SetEncart($pNomFichierEncart) {
 		if (($RefFichier = fopen($pNomFichierEncart, "r")) !== False) {
 			fgets($RefFichier);
 			for ($i=0; $i<11; $i++) {
 				fgetc($RefFichier);
 			}
 			$Titre = fgets($RefFichier);
			fgets($RefFichier);
 			$Contenu = "";
			$i=0;
			$Saut = true;
			while (!feof($RefFichier)) {
				// =====================================================================================================================================================
				// Insertion d'un saut de ligne XHTML
				// =====================================================================================================================================================
				if ($i <> 0 AND $Saut) {
					$Contenu .= "<br/>";
				}else {
					$i++;
				}
				
				// =====================================================================================================================================================
				// Lecture d'une ligne
				// =====================================================================================================================================================
				$DebutLigne = fgetc($RefFichier);		// On lit le premier caractère de la ligne
				if ($DebutLigne == "#") {
					$Saut = False; // On lit le reste de la ligne, c'est-à-dire à partir du second caractère puisque le premier caractère a déjà été lu avec la fonction fgetc
					$Contenu .= "<span class='Centrer'>".fgets($RefFichier)."</span>";
				}elseif ($DebutLigne == "*") {
					$Saut = true;
					$Contenu .= "<img alt='' src=\"".DIR_IMAGE_DIVERS."Boule.png\" /> ";
					$Contenu .= fgets($RefFichier);		// On lit le reste de la ligne, c'est-à-dire à partir du second caractère puisque le premier caractère a déjà été lu avec la fonction fgetc
				}else {
					$Saut = true;
					fseek($RefFichier, -1, SEEK_CUR);	// On déplace le curseur de position d'un caractère en arrière afin de ne pas "oublier" le caractère lu précédemment avec la fonction fgetc     
					$Contenu .= fgets($RefFichier);		// On lit la ligne entière, y compris le premier caractère
				}
				// if (trim($Titre) == "M�t�o") { // trim : Suppression des caractères invisibles en début et en fin de chaîne  /r/n...
			}
			fclose($RefFichier);
			$this->SetTitre($Titre);
			$this->SetContenu($Contenu);
		}else {
 			throw new Exception("Impossible de lire l'encart. Le fichier ".$pNomFichierEncart." est introuvable.", 0);
		}
 	}
	/**
	 * Renvoie le bloc XHTML relatif 0 l'encart
	 * @param null
	 * @return string : le bloc XHTML relatif à l'encart 
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	public function GetXhtmlEncart() {
		return "<div class='".$this->GetSelecteurCssEncart()."'>
					<span class='".$this->GetSelecteurCssTitre()."'>".$this->GetTitre()."</span>".
					$this->GetContenu().
			   "</div>\n";
	}
		
	/**
 	 * Méthode MAGIQUE appelée automatiquement lorsque l'utilisateur essaie d'afficher un objet de la classe. La méthode GetXhtmlEncart() est alors appelée.
 	 * @param null
 	 * @return string : le bloc XHTML relatif à l'encart 
 	 * @author : Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 	 * @version : 1.0
 	 * @copyright Christophe Goidin - Mai 2013
 	 */
 	public function __toString() {
        return $this->GetXhtmlEncart();
    }

} // fin class

?>