<?php
/*=============================================================================================================
	Fichier				: class.Collection.inc.php
	Auteur				: Christophe Goidin (christophe.goidin@ac-grenoble.fr)
	Date de création	: Mai 2013
	Date de modification: Décembre 2013
	                      Novembre 2015  -> optimisation du code (changement des noms de variables, suppression d'une méthode, ...)
	Rôle				: Décrit la classe Collection qui permet de gérer une collection d'éléments
===============================================================================================================*/

/**
 * La classe Collection permet de gérer une collection d'éléments
 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 * @version 1.0
 * @copyright Christophe Goidin - Mai 2013
 */
class Collection {
	
	// =====================================================================================================================================================
	// Les attributs
	// =====================================================================================================================================================
	private $LesElements;		// Le tableau où les éléments sont stockés
	private $TailleFixe;		// Détermine si la collection possède un nombre déterminé d'éléments
	private $TailleMax;			// Le nombre d'éléments maximum gérés par la collection
	
	
	// =====================================================================================================================================================
	// Le constructeur
	// =====================================================================================================================================================
	/**
	 * Le constructeur de la classe Collection permet d'initialiser tous les attributs de la classe
	 * @param booleen $pTailleFixe : true si la taille est fixe, false sinon (false est la valeur par défaut) 
	 * @param integer $pNbElements : le nombre maximum d'éléments qui sont gérés par la collection (valeur par défaut : 50)
	 * @return null
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	public function __construct($pTailleFixe = false, $pTailleMax = 50) {
		$this->LesElements = array();
		$this->TailleFixe = $pTailleFixe;
		$this->TailleMax = $pTailleMax;
	}

	
	// =====================================================================================================================================================
	// Les méthodes privées
	// =====================================================================================================================================================
	/**
	 * Renvoie un booléen indiquant si la taille de la collection est fixe ou non.
	 * @param null 
	 * @return booleen : true si la taille de la collection est fixe, false sinon. 
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	private function EstTailleFixe() {
		return $this->TailleFixe;
	}
	
	/**
	 * Renvoie le nombre maximum d'éléments qui sont gérés par la collection si sa taille est fixe
	 * @param null 
	 * @return integer : le nombre maximal d'éléments gérés par la collection si sa taille est fixe (-1 si la taille de la collection est illimitée)
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	private function GetTailleMax() {
		return ($this->EstTailleFixe() ? $this->TailleMax : -1);
	}
	
	/**
	 * Renvoie tous les éléments de la collection sous forme d'un tableau
	 * @param null 
	 * @return array : tous les éléments de la collection 
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	private function GetArrayElements() {
		return $this->LesElements;
	}
	
	/**
	 * Renvoie la clé correspondant à l'élément testé
	 * @param l'élément dont on veut récupérer la clé. Cet élément peut être de plusieurs types : string, object, ... 
	 * @return integer : La clé correspondant à l'élément passé en param�tre
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	private function GetCle($pUnElement) {
		return array_search($pUnElement, $this->GetArrayElements());
	}
		
	/**
	 * Détermine si un élément est présent ou non dans la collection
	 * @param l'élément dont la présence doit être testée dans la collection. Cet élément peut être de plusieurs types : string, object, ... 
	 * @return boolean : true si l'élément est présent dans la collection, false sinon
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	private function EstPresent($pUnElement) {
		return in_array($pUnElement, $this->GetArrayElements());
		/*$Cle = array_search($pUnElement, $this->GetArrayElements());
		if (!$Cle) {
			return false;
		}else {
			return true;
		}*/
	}
	
	
	// =====================================================================================================================================================
	// Les méthodes publiques
	// =====================================================================================================================================================
	/**
	 * Renvoie un booléen indiquant si la collection est pleine. La collection est considérée comme pleine si sa taille est fixe et si le nombre d'éléments la composant est égal au nombre maximal d'éléments
	 * @param null 
	 * @return boolean : true si la collection est pleine, false sinon. 
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	public function EstPlein() {
		return !(!$this->EstTailleFixe() Or ($this->EstTailleFixe() && $this->GetTaille() < $this->GetTailleMax()));
	}
	
	/**
	 * Renvoie le nombre d'éléments de la collection
	 * @param null 
	 * @return integer : le nombre d'éléments de la collection ou le booléen false s'il n'y a aucuns éléments
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	public function GetTaille() {
		//return count($this->LesElements);
		$NbElements = count($this->LesElements);
		return ($NbElements == 0 ? false : $NbElements);
	}
	
	/**
	 * teste si la collection est vide
	 * @param null 
	 * @return booleen : true si la collection est vide, false sinon
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Décembre 2013
	 */
	public function EstVide() {
		return $this->GetTaille() == 0;
	}
	
	/**
	 * Ajoute un élément à la collection
	 * @param l'élément à ajouter qui peut être de plusieurs types : string, object, ... 
	 * @return null
	 * @throws une exception est lancée si l'ajout d'un élément n'est pas possible en cas de collection pleine 
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	public function Add($pUnElement) {
		if (!$this->EstPlein()) {
			array_push($this->LesElements, $pUnElement); // ajoute un élément à la fin du tableau
			//$this->LesElements[] = $pUnElement;
		}else {
			throw new Exception("Impossible d'ajouter un élément à la collection. Le nombre maximal d'éléments est atteint.", 0);
		}
	}
		
	/**
	 * Retire un élément de la collection
	 * @param l'élément à retirer de la collection qui peut être de plusieurs types : string, object, ... 
	 * @return null
	 * @throws une exception est lancée si l'élément qu'on essait de retirer n'est pas présent dans la collection 
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	public function Retirer($pUnElement) {
		if ($this->EstPresent($pUnElement)) {
			unset($this->LesElements[$this->GetCle($pUnElement)]);
		}else  {
			throw new Exception("Impossible de retirer un élement non présent dans la collection.", 0);
		}
	}
	
	/**
	 * Renvoie tous les éléments de la collection sous forme d'une chaîne de caractères
	 * @param null 
	 * @return string : La liste des éléments sous forme d'une chaîne de caractères
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	public function GetListeElements() {
		$Result = "";
		foreach ($this->GetArrayElements() as $Cle => $UnElement) {
			$Result .= $UnElement; //."<br/>";
		}
		return $Result;
	}
		
} // fin class

?>