/*==================================================
 Fichier : ValidationSaisie.js
 Auteurs : BLANC-COQUAND Etienne, VITTI Clément
 Date de création : 15/12/2016
 ==================================================*/

//Détermine la duree du message en ms
var DureeMessage = 1000;

/**
 * Controle des caractères saisies dans le formulaire, accepte lettres majuscules et minuscules, chiffres, -, _, ., @
 * @param pCodeAscii : Le code ASCII a aller chercher et a comparer
 * @returns true = le caractere sera ecrit, false = le caractere sera ignoré avec message d'erreur
 */

function VerificationSaisie(pCodeAscii) {
    //Lettres majuscules
    if (pCodeAscii >= 65 && pCodeAscii <= 90) {
        return true;
    }
    //Lettres minuscules
    else if (pCodeAscii >= 97 && pCodeAscii <= 122) {
        return true;
    }
    //Chiffres
    else if (pCodeAscii >= 48 && pCodeAscii <= 57) {
        return true;
    }
    //Underscore, tiret, point et arobase et entrée
    else if (pCodeAscii == 95 || pCodeAscii == 45 || pCodeAscii == 46 || pCodeAscii == 64 || pCodeAscii == 13) {
        return true;
    }
    else {
        window.document.getElementById("ErreurSaisie").innerHTML = "Le caractère saisi n'est pas valide."
        setTimeout(function () {
            window.document.getElementById("ErreurSaisie").innerHTML = ""
        }, DureeMessage);
        return false;
    }
}

function VerificationSaisieModifPswd(pCodeAscii) {
    //Lettres majuscules
    if (pCodeAscii >= 65 && pCodeAscii <= 90) {
        return true;
    }
    //Lettres minuscules
    else if (pCodeAscii >= 97 && pCodeAscii <= 122) {
        return true;
    }
    //Chiffres
    else if (pCodeAscii >= 48 && pCodeAscii <= 57) {
        return true;
    }
    //Underscore, tiret, point et arobase et entrée
    else if (pCodeAscii == 95 || pCodeAscii == 45 || pCodeAscii == 46 || pCodeAscii == 64 || pCodeAscii == 13) {
        return true;
    }
    else {
        window.document.getElementById("ErreurSaisieModifPswd").innerHTML = "Le caractère saisi n'est pas valide."
        setTimeout(function () {
            window.document.getElementById("ErreurSaisieModifPswd").innerHTML = ""
        }, DureeMessage);
        return false;
    }
}

function ValidationSaisie(a) {
    var b = {
        TxtAncienMotDePasse: 20,
        TxtNouveauMotDePasse1: 20,
        TxtNouveauMotDePasse2: 20
    }
}

function VerifMotDePasse(a) {
    if (0 == a.TxtAncienMotDePasse.value.length)return Afficher("L'ancien mot de passe doit être renseigné"), a.TxtAncienMotDePasse.focus(), !1;
    if (0 == a.TxtNouveauMotDePasse1.value.length)return Afficher("Le nouveau mot de passe doit être renseigné 2 fois"), a.TxtNouveauMotDePasse1.focus(), !1;
    if (0 == a.TxtNouveauMotDePasse2.value.length)return Afficher("Le nouveau mot de passe doit être renseigné 2 fois"), a.TxtNouveauMotDePasse2.focus(), !1;
    if (a.TxtNouveauMotDePasse1.value.length != a.TxtNouveauMotDePasse2.value.length)return Afficher("Les 2 nouveaux mots de passe doivent comporter le même nombre de caractères"), a.TxtNouveauMotDePasse1.focus(), !1;
    if (a.TxtNouveauMotDePasse1.value != a.TxtNouveauMotDePasse2.value)return Afficher("Les 2 nouveaux mots de passe doivent être identiques"), a.TxtNouveauMotDePasse1.focus(), !1;
    if ("insignifiant" == window.document.getElementById("CouleurNiveauSecurite").innerHTML || "très insuffisant" == window.document.getElementById("CouleurNiveauSecurite").innerHTML || "insuffisant" == window.document.getElementById("CouleurNiveauSecurite").innerHTML || "faible" == window.document.getElementById("CouleurNiveauSecurite").innerHTML || "passable" == window.document.getElementById("CouleurNiveauSecurite").innerHTML)return Afficher("Le niveau de sécurité du mot de passe n'est pas suffisant"), a.TxtNouveauMotDePasse1.focus(), !1;
    var b = new JSEncrypt;
    return b.setPublicKey(vPublicKey), a.TxtAncienMotDePasse.style.color = "#EEEEF3", a.TxtAncienMotDePasse.value = b.encrypt(a.TxtAncienMotDePasse.value), a.TxtNouveauMotDePasse1.style.color = "#EEEEF3", a.TxtNouveauMotDePasse1.value = b.encrypt(a.TxtNouveauMotDePasse1.value), a.TxtNouveauMotDePasse2.value = "", !0
}