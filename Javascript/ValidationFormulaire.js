/*==================================================
Fichier : ValidationFormulaire.js
Auteurs : BLANC-COQUAND Etienne, VITTI Clément
Date de création : 08/12/2016
==================================================*/

//Détermine la duree du message
var DureeMessage = 1;

/**
 * Controle du remplissage des différentes zones de textes du formulaire d'authentification avant l'envoi et chiffrement de celui-ci
 * @param pForm : Le formulaire a controler
 * @returns true = le formulaire sera envoyé ou false = le formulaire ne sera pas envoyé
 */
function VerifFormulaireAuthentification(Form){
	if (Form.TextLogin.value == ""){
		window.document.getElementById("MessageErreur").innerHTML = "Votre login doit être renseigné" 
		setTimeout(function(){window.document.getElementById("MessageErreur").innerHTML = "" }, DureeMessage*1000);
		return false;
	}else if(Form.TextMdp.value.length == 0){
		window.document.getElementById("MessageErreur").innerHTML = "Votre mot de passe doit être renseigné";
		setTimeout(function(){window.document.getElementById("MessageErreur").innerHTML = "" }, DureeMessage*1000);
		return false;
	}else{
		var vJSEncrypt = new JSEncrypt();
		vJSEncrypt.setPublicKey(vPublicKey);
		Form.TextLogin.value = vJSEncrypt.encrypt(Form.TextLogin.value);
		Form.TextLogin.style.color = "white";
		Form.TextMdp.value = vJSEncrypt.encrypt(Form.TextMdp.value);
		Form.TextMdp.style.color = "white";
		return true;
	}
}