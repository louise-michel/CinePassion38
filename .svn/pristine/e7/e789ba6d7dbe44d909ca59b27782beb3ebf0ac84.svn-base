<?php
/*=============================================================================================================
 Fichier				: class.Navigation.inc.php
Auteur					: Etienne BLANC-COQUAND
Date de création		: Novembre 2016
Date de modification	:
Rôle					: La classe navigation permet de générer les boutons de navigations et les numéros de pages sous le tableau
===============================================================================================================*/

/**
 * La classe Navigation permet de gérer les boutons de navigation et les numéros de page
* @author BLANC-COQUAND Etienne
* @version 1.0
* @copyright BLANC-COQUAND Etienne
*/
class Navigation{
	private $Infos = array();
	// ==================================================================================================
	// Le constructeur
	// ==================================================================================================
	/**
	 * Le constructeur de la classe Navigation permet d'initialiser tous les attributs de la classe
	 * @param integer $pNumPage :
	 * @param integer $pNbPages :
	 * @param integer $pInfosNav :
	 * @return
	 * @author
	 * @version 1.0
	 * @copyright
	 */
	public function __construct($pPage, $pAction, $pInfosNav){
		$this->NavPremActif = 	DIR_IMAGE_DIVERS."NavPremActif.png";
		$this->NavPremInactif = DIR_IMAGE_DIVERS."NavPremInactif.png";
		$this->NavPremSurvol = 	DIR_IMAGE_DIVERS."NavPremSurvol.png";
		$this->NavDerActif = 	DIR_IMAGE_DIVERS."NavDerActif.png";
		$this->NavDerInactif = 	DIR_IMAGE_DIVERS."NavDerInactif.png";
		$this->NavDerSurvol = 	DIR_IMAGE_DIVERS."NavDerSurvol.png";
		$this->NavPrecActif = 	DIR_IMAGE_DIVERS."NavPrecActif.png";
		$this->NavPrecInactif = DIR_IMAGE_DIVERS."NavPrecInactif.png";
		$this->NavPrecSurvol = 	DIR_IMAGE_DIVERS."NavPrecSurvol.png";
		$this->NavSuivActif = 	DIR_IMAGE_DIVERS."NavSuivActif.png";
		$this->NavSuivInactif = DIR_IMAGE_DIVERS."NavSuivInactif.png";
		$this->NavSuivSurvol = 	DIR_IMAGE_DIVERS."NavSuivSurvol.png";
		$this->Page = $pPage;
		$this->Action = $pAction;

		if (!empty($pInfosNav)){
			foreach ($pInfosNav as $Cle => $Valeur){
				$this->$Cle = $Valeur;
			}
		}
	}

	// ==================================================================================================
	// Les accesseurs (ou getter)
	// ==================================================================================================
	/**
	 * Méthode MAGIQUE permettant de retourner la valeur de l'attribut $pNomAttribut dans le tableau $Page
	 * @param string $pNomAttribut : le nom de l'attribut
	 * @return string : la valeur correspondant à la clé $pNomAttribut dans le tableau $Page. Renvoie le booléen False si $pNomAttribut n'a pas été trouvé dans le tableau $Page
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	public function __get($pNomAttribut) {
		if (array_key_exists($pNomAttribut, $this->Infos)) {
			return $this->Infos[$pNomAttribut];
		}else {
			return false;
		}
	}


	// ==================================================================================================
	// Les mutateurs (ou setter)
	// ==================================================================================================
	/**
	 * Méthode MAGIQUE permettant d'alimenter le tableau $Page
	 * @param string $pNomAttribut : le nom de l'attribut
	 * @param string $pValeurAttribut : la valeur de l'attribut
	 * @return null
	 * @author Christophe Goidin <christophe.goidin@ac-grenoble.fr>
	 * @version 1.0
	 * @copyright Christophe Goidin - Mai 2013
	 */
	public function __set($pNomAttribut, $pValeurAttribut) {
		$this->Infos[$pNomAttribut] = $pValeurAttribut;
	}

	// ==================================================================================================
	// Les méthodes destinées à la génération du bloc XTML
	// ==================================================================================================
	/**
	 * Renvoie le bloc XHTML relatif à la page
	 * @param null
	 * @return string : le bloc XHTML relatif aux boutons et numéros de pages
	 * @author
	 * @version 1.0
	 * @copyright
	 */

	// ==================================================================================================
	// Génération des boutons
	// ==================================================================================================
	public function GetXhtmlBoutons(){
		if ($this->NumPage == 1){
			$ResultBoutons = "<img src='".$this->NavPremInactif."'>".
					"<img src='".$this->NavPrecInactif."'>".
					"<a href='./Index.php?Page=".$this->Page."&amp;Action=".$this->Action."&amp;NumPage=".($this->NumPage + 1)."'> <img id='ImageSuiv' src='".$this->NavSuivActif."' onmouseover='window.document.getElementById(\"ImageSuiv\").src=\"".$this->NavSuivSurvol."\"' onmouseout='window.document.getElementById(\"ImageSuiv\").src=\"".$this->NavSuivActif."\"'></a>".
					"<a href='./Index.php?Page=".$this->Page."&amp;Action=".$this->Action."&amp;NumPage=".$this->NbPages."'> <img id='ImageDer' src='".$this->NavDerActif."' onmouseover='window.document.getElementById(\"ImageDer\").src=\"".$this->NavDerSurvol."\"' onmouseout='window.document.getElementById(\"ImageDer\").src=\"".$this->NavDerActif."\"'></a>";

		} else if ($this->NumPage == $this->NbPages){
			$ResultBoutons = "<a href=./Index.php?Page=".$this->Page."&amp;Action=".$this->Action."&amp;NumPage=1> <img id='ImagePrem' src='".$this->NavPremActif."' onmouseover='window.document.getElementById(\"ImagePrem\").src=\"".$this->NavPremSurvol."\"' onmouseout='window.document.getElementById(\"ImagePrem\").src=\"".$this->NavPremActif."\"'></a>".
					"<a href='./Index.php?Page=".$this->Page."&amp;Action=".$this->Action."&amp;NumPage=".($this->NumPage - 1) ."'> <img id='ImagePrec' src='".$this->NavPrecActif."' onmouseover='window.document.getElementById(\"ImagePrec\").src=\"".$this->NavPrecSurvol."\"' onmouseout='window.document.getElementById(\"ImagePrec\").src=\"".$this->NavPrecActif."\"'></a>".
					"<img src='".$this->NavSuivInactif."'>".
					"<img src='".$this->NavDerInactif."'> </a>";

		} else{
			$ResultBoutons = "<a href='./Index.php?Page=".$this->Page."&amp;Action=".$this->Action."&amp;NumPage=1'> <img id='ImagePrem' src='".$this->NavPremActif."' onmouseover='window.document.getElementById(\"ImagePrem\").src=\"".$this->NavPremSurvol."\"' onmouseout='window.document.getElementById(\"ImagePrem\").src=\"".$this->NavPremActif."\"'></a>".
					"<a href='./Index.php?Page=".$this->Page."&amp;Action=".$this->Action.'&amp;NumPage='.($this->NumPage - 1) ."'> <img id='ImagePrec' src='".$this->NavPrecActif."' onmouseover='window.document.getElementById(\"ImagePrec\").src=\"".$this->NavPrecSurvol."\"' onmouseout='window.document.getElementById(\"ImagePrec\").src=\"".$this->NavPrecActif."\"'></a>".
					"<a href='./Index.php?Page=".$this->Page."&amp;Action=".$this->Action.'&amp;NumPage='.($this->NumPage + 1) ."'> <img id='ImageSuiv' src='".$this->NavSuivActif."' onmouseover='window.document.getElementById(\"ImageSuiv\").src=\"".$this->NavSuivSurvol."\"' onmouseout='window.document.getElementById(\"ImageSuiv\").src=\"".$this->NavSuivActif."\"'></a>".
					"<a href='./Index.php?Page=".$this->Page."&amp;Action=".$this->Action.'&amp;NumPage='.$this->NbPages."'> <img id='ImageDer' src='".$this->NavDerActif."' onmouseover='window.document.getElementById(\"ImageDer\").src=\"".$this->NavDerSurvol."\"' onmouseout='window.document.getElementById(\"ImageDer\").src=\"".$this->NavDerActif."\"'></a>";
		}
		return $ResultBoutons;
	}

	// ==================================================================================================
	// Génération des numéros de page
	// ==================================================================================================
	
	public function GetXhtmlNumeros() {
		$ResultNum = "";
		for($i=1;$i<=$this->NbPages;$i++){
			if ($i == $this->NumPage) {
				$ResultNum .= " <strong>$i</strong> ";
			} elseif ($i == 1 or $i == $this->NbPages or ($i >= $this->NumPage - NAV_NUM_AVAP and $i <= $this->NumPage + NAV_NUM_AVAP)) {
				$ResultNum .= "<a href=\"./Index.php?Page=".$this->Page."&Action=".$this->Action."&NumPage=".$i."\"> ".$i." </a>";
			} elseif ($i == 2 or $i == $this->NbPages - 1) {
				$ResultNum .= " ... ";
			}
		}
		return "Page : ".$ResultNum;
	}
}