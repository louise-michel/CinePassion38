<?php
class Bd {
	private static $instance = null;
	private static $PDOStat = null;
	 
	private static function GetInstance() {
		if (!self::$instance) {
			try {
	 			self::$instance = new PDO("mysql:host=".BD_SERVER.";dbname=".BD_NAME, BD_USER, BD_PWD,
	 			array(PDO::ATTR_PERSISTENT => true));
	 		}
	 		catch (PDOException $e) {
	 			die ("Echec __METHOD__ : " . $e->getMessage());
	 		}
	 	self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 		}
		return self::$instance;
	}
 
 /**
  * Renvoie le nombre de films présents dans la base
  * @param string $pTitreFilm : Représente le titre du film passé en paramètre
  * @return Le nombre total de films dans la base
  * @author Christophe Goidin
  * @version 1.0
  * @copyright Christophe Goidin
  */
	
	public static function GetNbFilms() {
		try {
			$Req = "SELECT COUNT(*) AS NbFilms FROM Film";
		 	$Result = self::GetInstance()->query($Req)->Fetch(PDO::FETCH_ASSOC);
		 	return $Result['NbFilms'];
	 	} 
	 	catch (Exception $e) {
	 		echo __METHOD__ . " : " . $e->getMessage();
	 	}
 	}
 
/**
* Renvoie le titre, la durée, la date, le libellé du genre du film, le prenom et nom du realisateur du film
* @param int pDebut : Représente le premier film
* @param int pNb : Représente le nombre de films à afficher
* @return Les informations concernant les films 
* @author Blanc-Coquand Etienne
* @version 1.0
* @copyright Blanc-Coquand Etienne
*/
	public static function GetListeFilms($pDebut, $pNb = NAV_NB_LIGNESPARPAGE) {
		try {
			$Req = "SELECT NumFilm, TitreFilm, DureeFilm, DATE_FORMAT(DateSortieFilm, '%Y') as Date, LibelleGenre, NomPersonne, PrenomPersonne
					FROM Film F
					INNER JOIN Genre G on F.NumGenreFilm = G.NumGenre
					INNER JOIN Realisateur R on F.NumRealisateurFilm = R.NumRealisateur
					INNER JOIN Personne P on R.NumRealisateur = P.NumPersonne
					ORDER BY 2 
					LIMIT $pDebut, $pNb";
			$Jeu = self::GetInstance()->query($Req);
			foreach ($Jeu as $Tuple) {
				$Result[]=$Tuple;
		 	}
		 	return $Result;
		}
		catch (Exception $e) {
			echo "Echec __METHOD__ : " . $e->getMessage();
		}
	}

 /**
  * Renvoie le numéro du film passé en paramètre
  * @param string $pTitreFilm : Représente le titre du film passé en paramètre
  * @return Le numéro du film dans la base.
  * @author Clement Vitti
  * @version 1.0
  * @copyright Clement Vitti
  */
 
	public static function GetNumFilm($pTitreFilm) {
		try {
			$Req="SELECT NumFilm FROM Film WHERE TitreFilm = '".$pTitreFilm."'";
			$Jeu=self::GetInstance()->query($Req);
			foreach ($Jeu as $Tuple) {
				$Result[]=$Tuple;
			}
			return $Result;
		} catch (Exception $e) {
			 echo "Echec __METHOD__ : " . $e->getMessage();
		 }
	}
 
 /**
  * Renvoie le numéro de la page où se situe le film renseigné
  * @param int $pNumFilm : Représente le numéro du film
  * @param int $pNbLignesParPages : Représente le nombre de films par page
  * @return Le numéro de la page du film
  * @author Clement Vitti
  * @version 1.0
  * @copyright Clement Vitti
  */
 
	public static function GetNumPage($pNumFilm, $pNbLignesParPages = NAV_NB_LIGNESPARPAGE) {
 		return ceil($pNumFilm / $pNbLignesParPages);
 	}
 	
 	/**
 	 * Renvoie les informations détaillées d'un film dont le numéro est passé en paramètre
 	 * @param int $pNumFilm : Représente le numéro du film
 	 * @return Les informations du film
 	 * @author Clement Vitti - Etienne Blanc-Coquand
 	 * @version 1.0
 	 * @copyright Clement Vitti - Etienne Blanc-Coquand
 	 */ 	
 	public static function GetDetailFilm($pNumFilm){
 		try{
 			$Req="	SELECT * FROM film f
					INNER JOIN genre g ON f.NumGenreFilm = g.NumGenre
					INNER JOIN pays p ON f.NumPaysFilm = p.NumPays
					INNER JOIN realisateur r ON f.NumRealisateurFilm = r.NumRealisateur
					INNER JOIN personne pe ON r.NumRealisateur = pe.NumPersonne
					INNER JOIN pays p2 ON pe.NumPaysPersonne = p2.NumPays
 					WHERE NumFilm = ".$pNumFilm;
	 		return self::getInstance()->query($Req)->Fetch(PDO::FETCH_ASSOC);
 		} catch (Exception $e) {
			 echo "Echec __METHOD__ : " . $e->getMessage();
		 }
 	}
 	
 	/**
 	 * Renvoie un tableau composé des informations détaillées des acteurs ayant participé au film dont le numéro est passé en paramètre
 	 * @param int $pNumFilm : Représente le numéro du film
 	 * @return Tableau avec infos détaillés des acteurs du film
 	 * @author Clement Vitti - Etienne Blanc-Coquand
 	 * @version 1.0
 	 * @copyright Clement Vitti - Etienne Blanc-Coquand
 	 */
 	public static function GetListeActeurs($pNumFilm){
 		
 		try{
 			$Req="	SELECT personne.NomPersonne, personne.PrenomPersonne, personne.DateNaissancePersonne, floor(datediff(CURRENT_DATE(),personne.DateNaissancePersonne)/365) as Age, personne.VilleNaissancePersonne, personne.SexePersonne, pays.LibellePays, participer.Role
 					from film
 					INNER join participer on participer.NumFilm = film.NumFilm
 					INNER JOIN acteur on participer.NumActeur = acteur.NumActeur
 					INNER JOIN personne on personne.NumPersonne = acteur.NumActeur
 					INNER JOIN pays on personne.NumPaysPersonne = pays.NumPays
 					WHERE film.NumFilm = ".$pNumFilm." Order by 1, 2";
 			return self::getInstance()->query($Req)->Fetch(PDO::FETCH_ASSOC);
 		} catch (Exception $e) {
 			echo "Echec __METHOD__ : " . $e->getMessage();
 		}
 	}
 	
 	/**
 	 * Renvoie un tableau constitué de la liste des films réalisés par un réalisateur dont le numéro est passé en paramètre
 	 * @param int $pNumRealisateur : Représente le numéro du réalisateur
 	 * @return Tableau avec liste de films d'un réalisateur
 	 * @author Clement Vitti - Etienne Blanc-Coquand
 	 * @version 1.0
 	 * @copyright Clement Vitti - Etienne Blanc-Coquand
 	 */
 	public static function GetListeFilmsParRealisateur($pNumRealisateur){

 		try{
 			$Req="	SELECT film.TitreFilm, year(film.DateSortieFilm) as annee, month(film.DateSortieFilm) as mois
 					from film
 					INNER JOIN realisateur on realisateur.NumRealisateur = film.NumRealisateurFilm
 					INNER JOIN personne on personne.NumPersonne = realisateur.NumRealisateur
 					where personne.NumPersonne = ".$pNumRealisateur."
 					order by annee desc, mois desc";
 			return self::getInstance()->query($Req)->Fetch(PDO::FETCH_ASSOC);
 		} catch (Exception $e) {
 			echo "Echec __METHOD__ : " . $e->getMessage();
 		}
 	}
 	
 	/**
 	 * Renvoie la chaine de caractères contenant la clé publique
 	 * @param 
 	 * @return La chaine de caractère de la clé publique
 	 * @author Clement Vitti
 	 * @version 1.0
 	 * @copyright Clement Vitti
 	 */
 	
 	public static function GetRsaPublicKey($pNum) {
	 	try{
	 			$Req = "SELECT RsaPublicKey 
	 					from rsa 
	 					where RsaNum = ".$pNum;
	 			return self::getInstance()->query($Req)->Fetch(PDO::FETCH_ASSOC)['RsaPublicKey'];
	 		} catch (Exception $e) {
	 			echo "Echec __METHOD__ : " . $e->getMessage();
	 		}
 	}
 	
 	/**
 	 * Renvoie la chaine de caractères contenant la clé privée
 	 * @param
 	 * @return La chaine de caractère de la clé privée
 	 * @author Clement Vitti
 	 * @version 1.0
 	 * @copyright Clement Vitti
 	 */
 	
 	public static function GetRsaPrivateKey($pNum) {
	 	try{
	 			$Req = "SELECT RsaPrivateKey 
	 					from rsa 
	 					where RsaNum = ".$pNum;
	 			return self::getInstance()->query($Req)->Fetch(PDO::FETCH_ASSOC)['RsaPrivateKey'];
	 		} catch (Exception $e) {
	 			echo "Echec __METHOD__ : " . $e->getMessage();
	 		}
 	}
 	
 	/**
 	 * Renvoie la position du film dont le numéro est passé en second paramètre parmi tous les films réalisés par le réalisateur dont le numéro est passé en premier paramètre.
 	 * @param int $pNumRealisateur : Représente le numéro du réalisateur
 	 * @param int $pNumFilm : Représente le numéro du film
 	 * @return Position du film
 	 * @author Clement Vitti - Etienne Blanc-Coquand
 	 * @version 1.0
 	 * @copyright Clement Vitti - Etienne Blanc-Coquand
 	 */
 	public static function GetPositionFilmParRealisateur($pNumRealisateur, $pNumFilm){
 		
 	}
 	
 	/**
 	 * Renvoie toutes les informations concernant un utilisateur s'il est renseigné dans la base de données et s'il s'est authentifié
 	 * @param $pLoginUser : Le nom de l'utilisateur
 	 * @param $pPasswdUser : Le mot de passe de l'utilisateur
 	 * @return Retourne  un tableau avec les informations sur l'utilisateur, son login, son nom, son prenom, son sexe, le nombre d'echec de connexion, le nombre total de connexion, la date et l'heure de la dernière connexion et de création du compte et le type d'utilisateur
 	 * @author Clement Vitti - Etienne Blanc-Coquand
 	 * @version 1.0
 	 * @copyright Clement Vitti - Etienne Blanc-Coquand
 	 */
 	/*public static function GetResultAuthentification($pLoginUser, $pPasswdUser){
 		try {
 			$Req ="SELECT LoginUser, PrenomUser, NomUser, SexeUser, NbEchecConnexionUser, NbTotalConnexionUser, DateHeureDerniereConnexionUser, DateHeureCreationUser, LibelleTypeUser FROM user U INNER JOIN typeuser T ON U.TypeUser = T.NumTypeUser WHERE LoginUser = ':UnLoginUser' AND PasswdUser = ':UnPasswdUser'";
 			$PDOStat = self::GetInstance()->prepare($Req);
 			
 			$LeLogin = $pLoginUser;
 			$LePasswd = hash('sha512', $pPasswdUser);   
 			$PDOStat->bindParam(':UnLoginUser', $LeLogin);   
 			$PDOStat->bindParam(':UnPasswdUser', $LePasswd);   
 			$PDOStat->execute();
 			return $PDOStat->Fetch(PDO::FETCH_ASSOC);
 			
 		} catch (Exception $e) {
 			echo "Echec __METHOD__ : " . $e->getMessage();
 		}
 	}*/
 	
 	public static function GetResultAuthentification($pLoginUser, $pPasswdUser) {
 		try {
 				
 			$Req="SELECT LoginUser, PrenomUser, NomUser, SexeUser, NbEchecConnexionUser, NbTotalConnexionUser, DateHeureDerniereConnexionUser, LibelleTypeUser, DateHeureCreationUser FROM user, typeuser WHERE LoginUser=:unLogin AND PasswdUser=:unPasswd AND typeuser.NumTypeUser=user.TypeUser";
 			self::$PDOStat = self::getInstance()->prepare($Req);
 			$Login = $pLoginUser;
 			$Passwd = hash("sha512", $pPasswdUser);
 			self::$PDOStat->bindParam(':unLogin', $Login, PDO::PARAM_STR, 20);
 			self::$PDOStat->bindParam(':unPasswd', $Passwd, PDO::PARAM_STR, 128);
 			self::$PDOStat->execute();
 			return self::$PDOStat->Fetch(PDO::FETCH_ASSOC);
 	
 		} catch (Exception $e) {
 			echo "Echec GetResultAuthentification() : " . $e->getMessage();
 		}
 	}
 	
 	public static function SetNbEchecConnexion($pLoginUser, $pOperation){
 		if($pOperation == "incrementer"){
 			$Ope = " NbEchecConnexionUser + 1";
 		}
 		else{
 			$Ope = "0";
 		}
 		try {
 			$Req="Update User Set NbEchecConnexionUser =".$Ope." WHERE LoginUser='".$pLoginUser."'"; 		
 			return self::getInstance()->query($Req);
 		} 
 		catch (Exception $e) {
 			echo "Echec __METHOD__ : " . $e->getMessage();
 		}
 	}
 	
 	public static function SetNbTotalConnexion($pLoginUser){
 		try {
 			$Req="Update User Set NbTotalConnexionUser = NbTotalConnexionUser + 1 WHERE LoginUser='".$pLoginUser."'";
 			return self::getInstance()->query($Req);
 		}
 		catch (Exception $e) {
 			echo "Echec __METHOD__ : " . $e->getMessage();
 		}
 	}
 	
 	public static function SetDateHeureDerniereConnexion($pLoginUser){
 		try {
 			$Req="Update User Set DateHeureDerniereConnexionUser = Sysdate() WHERE LoginUser='".$pLoginUser."'";
 			return self::getInstance()->query($Req);
 		}
 		catch (Exception $e) {
 			echo "Echec __METHOD__ : " . $e->getMessage();
 		}
 	}
 	}
 	
?>