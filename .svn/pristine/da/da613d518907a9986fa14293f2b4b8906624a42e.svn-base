/* ======================================================================================================
 * Feuille de style		: MiseEnForme.css
 * Objectif(s)			: Mise en forme générale du site
 * Auteur 				: Christophe Goidin <christophe.goidin@ac-grenoble.fr>
 * Date de création		: Juillet 2012
 * Date de modification : 
 * ======================================================================================================
 */

body {
	text-align: justify;
	font-family: 'Raleway', sans-serif;
	color: #404040;
	background-image: linear-gradient(bottom, rgb(255,255,255) 29%, rgb(20,20,36) 86%);
	background-image: -o-linear-gradient(bottom, rgb(255,255,255) 29%, rgb(20,20,36) 86%);
	background-image: -moz-linear-gradient(bottom, rgb(255,255,255) 29%, rgb(20,20,36) 86%);
	background-image: -webkit-linear-gradient(bottom, rgb(255,255,255) 29%, rgb(20,20,36) 86%);
	background-image: -ms-linear-gradient(bottom, rgb(255,255,255) 29%, rgb(20,20,36) 86%);
	background-image: -webkit-gradient(linear,left bottom,left top,color-stop(0.29, rgb(255,255,255)),color-stop(0.86, rgb(20,20,36)));
}

/* ======================================================================================================= 
   Images
   ======================================================================================================= */
img {
	border: none;
}

img.LogoPartenaire {
	width: 200px;
	height: auto;
}

div#BandeauHaut img {
	border-radius: 10px;
}

div#BandeauHaut div#Verrou{
    position: absolute;
    top: -33px;
    left: 200px;
    width: 40px;
}

div#BandeauHaut div#CleAdmin{
    position: absolute;
    top: -27px;
    left: 240px;
    width: 40px;
}

div#BandeauHaut div#MessageVerrou{
    position: absolute;
    text-align: center;
    top: -23px;
    left: 60px;
    width: 140px;
    color: #000000;
    border-radius: 5px;
    background-color: #BB963E;
}

div#Certification img {
	border-bottom-right-radius: 10px;
	border-bottom-left-radius: 10px;
}

div#Certification #W3C img {
	border-radius: 0px;
}

img#RetourHautDePage {
	position: fixed;
	visibility: hidden;
	right: 37px;
	bottom: 100px;
	z-index: 100;
}


/* ======================================================================================================= 
   Liens hypertexte
   ======================================================================================================= */
a {
	text-decoration: none;
}

div#Contenu a,
div#Liens a,
div#BandeauHaut div#CheminDeFer a,
div#Pied ul li a,
div#Authentification a {
	color: inherit;		/* les liens auront la même couleur que le texte du bloc conteneur -> héritage de la couleur des blocs <Contenu>, <Liens>, <BandeauHaut>, <CheminDeFer>, <Pied> et <Authentification> */
}

div#Contenu a.Souligne,
div#Liens a:hover,
div#BandeauHaut div#CheminDeFer a:hover,
div#Authentification a:hover {
	text-decoration: underline;
}

div#Contenu a.Souligne:hover {
	color: black;
}

div#Contenu div#BlocGauche div.Encart a:hover,
div#Contenu div#BlocDroite div.Encart a:hover {
	color: black;
	text-decoration: underline;
}

div#Pied ul li a:hover {
	color: #404040;
}

/* ======================================================================================================= 
   Navigation
   ======================================================================================================= */

div#Contenu div#BlocCentre div#Navigation{
	border: solid 1px #7f8691;
	border-radius: 10px;
}


/* ======================================================================================================= 
   Pied de page
   ======================================================================================================= */
div#Pied ul {
	margin-top: 0px;
	padding-left: 15px; /* car les blocs <PiedCol...> ont une marge gauche de 10px et elle n'est pas prise en compte -> sinon <ul> "déborde" du bloc */
	text-align: left;
}

div#Pied ul li {
	margin-left: 0px;
	padding-left: 0px;
}

div#Pied ul li:hover {
	color: #404040;		/* changement de la couleur du symbole de la liste à puce. Couleur identique au lien hypertexte */
}

/* ======================================================================================================= 
   Listes à puces
   ======================================================================================================= */
div#Contenu ul {
	margin-top: 0px;
}


/* ======================================================================================================= 
   Divers
   ======================================================================================================= */
.PlusVisible {
	font-weight: bolder;
	color: black;
}

div#Contenu span#TitrePage {
	display: block;
	width: 95%;
	margin-bottom: 10px;
}

div#Contenu div#BlocCentre span.RubriqueTitre {
	display: block;
	text-align: left;
	font-weight : bolder;
	color: #404360;
}

div#Contenu div#BlocCentre span.RubriqueInfos {
	display: block;
	margin-bottom: 40px;
	text-align: justify;
	color: inherit;
}

div.FormModifMdp input#AncienMDP, div.FormModifMdp input#NouveauMDP, div.FormModifMdp input#ResaisirMDP{
	float: left;
}

div.FormModifMdp label{
	display: block;
	
}

.Italique {
	font-style: italic;
}

.Centrer {
	display: block;
	text-align: center;
}

.Noir {
	color: black;	
}

.Gras {
	font-weight: bolder;
}
