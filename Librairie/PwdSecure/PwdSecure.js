/*!
 * PwdSecure Library v1.0.0
 *
 * Copyright 2017, BLANC-COQUAND Etienne, VITTI Clement
 */
$(document).ready(function PositionnementDesBlocs() {
    var divPswd = $('#pwdSecure'),
        spanNivSec = $("#niveauSecurite"),
        spanCouleur = $("#CouleurNiveauSecurite"),
        divBarre = $('#barreSecurite'),
        spanVoyant = $("#voyantSecurite"),
        spanTest = $('#testEgalite');
});

$(document).ready(function TesterNiveauSecuriteMotDePasse(pTexte) {
    var divPswd = $('#pwdSecure'),
        spanNivSec = $("#niveauSecurite"),
        spanCouleur = $("#CouleurNiveauSecurite"),
        divBarre = $('#barreSecurite'),
        spanVoyant = $("#voyantSecurite"),
        spanTest = $('#testEgalite');

    $('#NouveauMDP').keyup(function () {
        var pswd = $(this).val();

        if(pswd.length > 0){
            $(divPswd).css('visibility', 'visible');
            $(spanVoyant).css('width', pswd.length * 10);
        } else {
            $(divPswd).css('visibility', 'hidden');
        }
    })
});


$(document).ready(function TesterEgaliteMotDePasse(pTexte1,pTexte2) {
    var pTexte1 = $('#NouveauMDP').val(),
        pTexte2 = $('#ResaisirMDP').val();

    $(pTexte1).keyup(function () {
        if(pTexte1 == pTexte2){
            // document.getElementById('testEgalite').innerHTML = "Egal";
            $('#testEgalite').html('Egal');
        }else{
            // document.getElementById('testEgalite').innerHTML = "Egal";
            $('#testEgalite').html('PAS EGAL');
        }
    });

    $(pTexte2).keyup(function () {
        if(pTexte1 == pTexte2){
            // document.getElementById('testEgalite').innerHTML = "Egal";
            $('#testEgalite').html('Egal');
        }else{
            // document.getElementById('testEgalite').innerHTML = "Egal";
            $('#testEgalite').html('PAS EGAL');
        }
    });
});